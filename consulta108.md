# 108. Disparador de actualización a nivel de fila (update trigger)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica un registro en la tabla "libros".

Eliminamos las tablas:

```sql
drop table control;
drop table libros;
```

Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un disparador que se dispare cada vez que se actualice un registro en "libros"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "libros":

```sql
create or replace trigger tr_actualizar_libros
    before update
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_libros;
/
```

Actualizamos varios registros de "libros". Aumentamos en un 10% el precio de todos los libros de editorial "Nuevo siglo':

```sql
update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';
```

Veamos cuántas veces se disparó el trigger consultando la tabla "control":

```sql
select *from control;
```

El trigger se disparó 2 veces, una vez por cada registro modificado en "libros". Si el trigger hubiese sido creado a nivel de sentencia, el "update" anterior hubiese disparado el trigger 1 sola vez aún cuando se modifican 2 filas.

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_LIBROS';
```

obtenemos el nombre del disparador, el momento y nivel (before each row), evento que lo dispara (update), a qué objeto está asociado (table), nombre de la tabla al que está asociado (libros) y otras columnas que no analizaremos por el momento.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table control;
drop table libros;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_actualizar_libros
    before update
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_libros;
/
 
update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';

select *from control;
 
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_LIBROS';
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada vez que un empleado actualiza datos sobre la tabla "empleados".

1. Elimine las tablas:

```sql
drop table empleados;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

3. Ingrese algunos registros en "empleados":

```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);
```

4. Cree un disparador que se dispare cada vez que se actualice un registro en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "update" sobre "empleados"
```sql
create or replace trigger tr_empleados
before update on empleados
for each row
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
5. Actualice varios registros de "empleados". Aumentamos en un 10% el sueldo de todos los empleados de la seccion "Secretaria'
```sql
update empleados set sueldo = sueldo * 1.1 where seccion = 'Secretaria';

```
6. Vea cuántas veces se disparó el trigger consultando la tabla "control"
```sql
select * from control;

```
7. El trigger se disparó 2 veces, una vez por cada registro modificado en "empleados". Si el trigger hubiese sido creado a nivel de sentencia, el "update" anterior hubiese disparado el trigger 1 sola vez aún cuando se modifican 2 filas.
```sql
SELECT trigger_name, trigger_type, triggering_event, table_owner, table_name
FROM user_triggers
WHERE table_name = 'EMPLEADOS';

```
8. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
obtenemos el nombre del disparador, el momento y nivel (before each row), evento que lo dispara (update), a qué objeto está asociado (table), nombre de la tabla al que está asociado (empleados) y otras columnas que no analizaremos por el momento.
```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'EMPLEADOS';

```