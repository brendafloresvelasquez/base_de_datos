# 49. Combinación externa derecha (right join)

## Practica de laboratorio

Una librería almacena la información de sus libros para la venta en dos tablas, "libros" y "editoriales".
Eliminamos ambas tablas, las creamos y agregamos dos restricciones "primary key" sobre los campos "codigo" de las dos tablas:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);

alter table libros
add constraint PK_libros
primary key(codigo);

alter table editoriales
add constraint PK_editoriales
primary key(codigo);
```

Ingresamos algunos registros en ambas tablas:

```sql
insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(4,'Norma');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',1);
insert into libros values(102,'Aprenda PHP','Mario Molina',2);
insert into libros values(103,'Java en 10 minutos',null,4);
insert into libros values(104,'El anillo del hechicero','Carol Gaskin',4);
```

Solicitamos el título y nombre de la editorial de los libros empleando un "right join":

```sql
select titulo,nombre as editorial
from libros l
right join editoriales e
on codigoeditorial = e.codigo;
```

Las editoriales de las cuales no hay libros, es decir, cuyo código de editorial no está presente en "libros" aparece en el resultado, pero con el valor "null" en el campo "titulo"; caso de la editorial "Siglo XXI".

Realizamos la misma consulta anterior agregando un "where" que restringa el resultado considerando solamente los registros que encuentran coincidencia en la tabla izquierda:

```sql
select titulo,nombre as editorial
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is not null;
```

Ya no aparece la editorial "Siglo XXI".

Mostramos las editoriales que NO están presentes en "libros" (que NO encuentran coincidencia en "editoriales"):

```sql
select nombre
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is null;
```

Solamente aparece la editorial "Siglo XXI".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);

alter table libros
add constraint PK_libros
primary key(codigo);

alter table editoriales
add constraint PK_editoriales
primary key(codigo);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(4,'Norma');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',1);
insert into libros values(102,'Aprenda PHP','Mario Molina',2);
insert into libros values(103,'Java en 10 minutos',null,4);
insert into libros values(104,'El anillo del hechicero','Carol Gaskin',4);

select titulo,nombre as editorial
from libros l
right join editoriales e
on codigoeditorial = e.codigo;

select titulo,nombre as editorial
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is not null;

select nombre
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is null;
```

## Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias" y créelas:

```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);
```

2. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Corrientes');
insert into clientes values (101,'Lopez Marcos','Colon 111','Córdoba',1);
insert into clientes values (102,'Perez Ana','San Martin 222','Cruz del Eje',1);
insert into clientes values (103,'Garcia Juan','Rivadavia 333','Villa Maria',1);
insert into clientes values (104,'Perez Luis','Sarmiento 444','Rosario',2);
insert into clientes values (105,'Gomez Ines','San Martin 666','Santa Fe',2);
insert into clientes values (106,'Torres Fabiola','Alem 777','La Plata',4);
insert into clientes values (107,'Garcia Luis','Sucre 475','Santa Rosa',5);
```

3. Muestre todos los datos de los clientes, incluido el nombre de la provincia empleando un "right join".
```sql
SELECT c.codigo, c.nombre, c.domicilio, c.ciudad, p.nombre AS provincia_nombre
FROM provincias p
RIGHT JOIN clientes c ON c.codigoprovincia = p.codigo;

```
4. Obtenga la misma salida que la consulta anterior pero empleando un "left join".
```sql
SELECT c.codigo, c.nombre, c.domicilio, c.ciudad, p.nombre AS provincia_nombre
FROM clientes c
LEFT JOIN provincias p ON c.codigoprovincia = p.codigo;

```
5. Empleando un "right join", muestre solamente los clientes de las provincias que existen en "provincias" (5 registros)
```sql
SELECT c.codigo, c.nombre, c.domicilio, c.ciudad, p.nombre AS provincia_nombre
FROM provincias p
RIGHT JOIN clientes c ON c.codigoprovincia = p.codigo
WHERE p.codigo IS NOT NULL;

```
6. Muestre todos los clientes cuyo código de provincia NO existe en "provincias" ordenados por ciudad (2 registros)
```sql
SELECT c.codigo, c.nombre, c.domicilio, c.ciudad, p.nombre AS provincia_nombre
FROM clientes c
LEFT JOIN provincias p ON c.codigoprovincia = p.codigo
WHERE p.codigo IS NULL
ORDER BY c.ciudad;

```