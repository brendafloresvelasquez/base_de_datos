# 58. Restricciones foreign key en la misma tabla

## Practica de laboratorio

Una mutual almacena los datos de sus afiliados en una tabla llamada "afiliados". Algunos afiliados inscriben a sus familiares. La tabla contiene un campo que hace referencia al afiliado que lo incorporó a la mutual, del cual dependen.

Eliminamos la tabla "afiliados" y la creamos:

```sql
drop table afiliados;

create table afiliados(
    numero number(5),
    documento char(8) not null,
    nombre varchar2(30),
    afiliadotitular number(5),
    primary key (documento),
    unique (numero)
);
```

En caso que un afiliado no haya sido incorporado a la mutual por otro afiliado, el campo "afiliadotitular" almacenará "null".

Establecemos una restricción "foreign key" para asegurarnos que el número de afiliado que se ingrese en el campo "afiliadotitular" exista en la tabla "afiliados":

```sql
alter table afiliados
add constraint FK_afiliados_afiliadotitular
foreign key (afiliadotitular)
references afiliados (numero);
```

Ingresamos algunos registros:

```sql
insert into afiliados values(1,'22222222','Perez Juan',null);
insert into afiliados values(2,'23333333','Garcia Maria',null);
insert into afiliados values(3,'24444444','Lopez Susana',null);
insert into afiliados values(4,'30000000','Perez Marcela',1);
insert into afiliados values(5,'31111111','Garcia Luis',2);
insert into afiliados values(6,'32222222','Garcia Maria',2);
```

Podemos eliminar un afiliado, siempre que no haya otro afiliado que haga referencia a él en "afiliadotitular", es decir, si el "numero" del afiliado está presente en algún registro en el campo "afiliadotitular":

```sql
delete from afiliados where numero=5;
```

Veamos la información referente a "afiliados":

```sql
select constraint_name, constraint_type,search_condition
from user_constraints
where table_name='AFILIADOS';
```

Aparece la siguiente tabla:

```sh
CONSTRAINT_NAME                CONSTRAINT_TYPE        SEARCH_CONDITION
---------------------------------------------------------------------------------
SYS_C004816                    C                      "DOCUMENTO" IS NOT NULL
SYS_C004817                    P
SYS_C004818                    U
FK_AFILIADOS_AFILIADOTITULAR   R
```

Los nombres de las tres primeras restricciones son dadas por Oracle.

La tabla tiene una restricción "check", una "primary key", una "unique" y una "foreign key".

Veamos sobre qué campos están establecidas:

```sql
select *from user_cons_columns
where table_name='AFILIADOS';
```

Aparece la siguiente tabla:

```sh
CONSTRAINT_NAME			COLUMN_NAME	POSITION
----------------------------------------------------------
SYS_C004818			NUMERO		1
SYS_C004817			DOCUMENTO	1
SYS_C004816			DOCUMENTO	
FK_AFILIADOS_AFILIADOTITULAR	AFILIADOTITULAR	1
```

Nos informa que la restricción única está establecida sobre "numero"; la "primary key" sobre "documento", la restricción de chequeo sobre "documento" y la "foreign key" sobre "afiliadotitular".

Ingresamos un nuevo registro con un valor para "afiliadotitular" existente:

```sql
insert into afiliados values(7,'33333333','Lopez Juana',3);
```

Intentamos ingresar un nuevo registro con un valor para "afiliadotitular" inexistente:

```sql
insert into afiliados values(8,'34555666','Marconi Julio',9);
```

Oracle no lo permite porque se violaría la restricción "foreign key".

Igresamos un nuevo registro con el valor "null" para "afiliadotitular":

```sql
insert into afiliados values(8,'34555666','Marconi Julio',null);
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table afiliados;

create table afiliados(
    numero number(5),
    documento char(8) not null,
    nombre varchar2(30),
    afiliadotitular number(5),
    primary key (documento),
    unique (numero)
);

alter table afiliados
add constraint FK_afiliados_afiliadotitular
foreign key (afiliadotitular)
references afiliados (numero);

insert into afiliados values(1,'22222222','Perez Juan',null);
insert into afiliados values(2,'23333333','Garcia Maria',null);
insert into afiliados values(3,'24444444','Lopez Susana',null);
insert into afiliados values(4,'30000000','Perez Marcela',1);
insert into afiliados values(5,'31111111','Garcia Luis',2);
insert into afiliados values(6,'32222222','Garcia Maria',2);

delete from afiliados where numero=5;

select constraint_name, constraint_type,search_condition
from user_constraints
where table_name='AFILIADOS';

select *from user_cons_columns
where table_name='AFILIADOS';

insert into afiliados values(7,'33333333','Lopez Juana',3);

insert into afiliados values(8,'34555666','Marconi Julio',9);

insert into afiliados values(8,'34555666','Marconi Julio',null);
```

## Ejercicios propuestos

Una empresa registra los datos de sus clientes en una tabla llamada "clientes". Dicha tabla contiene un campo que hace referencia al cliente que lo recomendó denominado "referenciadopor". Si un cliente no ha sido referenciado por ningún otro cliente, tal campo almacena "null".

1. Elimine la tabla y créela:

```sql
drop table clientes;
 
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    referenciadopor number(5),
    primary key(codigo)
);
```

2. Ingresamos algunos registros:

```sql
insert into clientes values (50,'Juan Perez','Sucre 123','Cordoba',null);
insert into clientes values(90,'Marta Juarez','Colon 345','Carlos Paz',null);
insert into clientes values(110,'Fabian Torres','San Martin 987','Cordoba',50);
insert into clientes values(125,'Susana Garcia','Colon 122','Carlos Paz',90);
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);
```

3. Intente agregar una restricción "foreign key" para evitar que en el campo "referenciadopor" se ingrese un valor de código de cliente que no exista.
No se permite porque existe un registro que no cumple con la restricción que se intenta establecer.
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_referenciadopor FOREIGN KEY (referenciadopor) REFERENCES clientes(codigo);

```
4. Cambie el valor inválido de "referenciadopor" del registro que viola la restricción por uno válido:

```sql
update clientes set referenciadopor=90 where referenciadopor=9;
```

5. Agregue la restricción "foreign key" que intentó agregar en el punto 3
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_referenciadopor FOREIGN KEY (referenciadopor) REFERENCES clientes(codigo);

```
6. Vea la información referente a las restricciones de la tabla "clientes"
La tabla tiene 2 restricciones.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
7. Intente agregar un registro que infrinja la restricción.
```sql
INSERT INTO clientes VALUES (200, 'Lucia Martinez', 'Bolivar 456', 'Cordoba', 300);

```
8. Intente modificar el código de un cliente que está referenciado en "referenciadopor"
```sql
UPDATE clientes SET codigo = 150 WHERE codigo = 110;

```
9. Intente eliminar un cliente que sea referenciado por otro en "referenciadopor"
```sql
DELETE FROM clientes WHERE codigo = 90;

```
10. Cambie el valor de código de un cliente que no referenció a nadie.
```sql
UPDATE clientes SET codigo = 210 WHERE codigo = 50;

```
11. Elimine un cliente que no haya referenciado a otros.
```sql
DELETE FROM clientes WHERE codigo = 140;

```