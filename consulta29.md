# 28. Contar registros (count)

## Ejercicio de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar(20),
    precio number(6,2)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values('El aleph','Borges','Emece',15.90);

insert into libros
values('Antología poética',null,'Planeta',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,40);

insert into libros
values('Aprenda PHP',default,'Nuevo siglo',null);

insert into libros
values('Uno','Richard Bach','Planeta',20);
```

Averiguemos la cantidad de libros usando la función "count()":

```sql
select count(*)
from libros;
```

Note que incluye todos los libros aunque tengan valor nulo en algún campo.

Contamos los libros de editorial "Planeta":

```sql
select count(*)
from libros
where editorial='Planeta';
```

Existen 2 libros de editorial "Planeta".

Contamos los registros que tienen precio (sin tener en cuenta los que tienen valor nulo), usando la función "count(precio)":

```sql
select count(precio)
from libros;
```

La consulta nos retorna el valor 5.

Contamos los registros que tienen valor diferente de "null" en "editorial":

```sql
select count(editorial)
from libros;
```

La consulta nos retorna el valor 5.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar(20),
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece',15.90);

insert into libros
values('Antología poética',null,'Planeta',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,40);

insert into libros
values('Aprenda PHP',default,'Nuevo siglo',null);

insert into libros
values('Uno','Richard Bach','Planeta',20);

select count(*)
from libros;

select count(*)
from libros
where editorial='Planeta';

select count(precio)
from libros;

select count(editorial)
from libros;
```

## Ejercicios propuestos

## Ejercicio 01

Trabaje con la tabla llamada "medicamentos" de una farmacia.

1. Elimine la tabla:

```sql
drop table medicamentos;
```

2. Cree la tabla con la siguiente estructura:

```sql
create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(3),
    fechavencimiento date not null,
    numerolote number(6) default null,
    primary key(codigo)
);
```

3. Ingrese algunos registros:

```sql
insert into medicamentos
values(120,'Sertal','Roche',5.2,1,'01/02/2015',123456);

insert into medicamentos
values(220,'Buscapina','Roche',4.10,3,'01/02/2016',null);

insert into medicamentos
values(228,'Amoxidal 500','Bayer',15.60,100,'01/05/2017',null);

insert into medicamentos
values(324,'Paracetamol 500','Bago',1.90,20,'01/02/2018',null);

insert into medicamentos
values(587,'Bayaspirina',null,2.10,null,'01/12/2019',null);

insert into medicamentos
values(789,'Amoxidal jarabe','Bayer',null,null,'15/12/2019',null);
```

4. Muestre la cantidad de registros empleando la función "count(*)" (6 registros)
```sql
SELECT COUNT(*)
FROM medicamentos;

```
5. Cuente la cantidad de medicamentos que tienen laboratorio conocido (5 registros)
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE laboratorio IS NOT NULL;

```
6. Cuente la cantidad de medicamentos que tienen precio y cantidad, con alias (5 y 4 respectivamente)
```sql
SELECT COUNT(precio) AS cantidad_precios, COUNT(cantidad) AS cantidad_cantidades
FROM medicamentos;

```
7. Cuente la cantidad de remedios con precio conocido, cuyo laboratorio comience con "B" (2 registros)
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE precio IS NOT NULL AND laboratorio LIKE 'B%';

```
8. Cuente la cantidad de medicamentos con número de lote distinto de "null" (1 registro)
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE numerolote IS NOT NULL;

```
9. Cuente la cantidad de medicamentos con fecha de vencimiento conocida (6 registros)
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE fechavencimiento IS NOT NULL;

```