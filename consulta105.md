# 105. Disparador de insercion a nivel de fila (insert trigger for each row)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y en una tabla "ofertas", algunos datos de los libros cuyo precio no supera los $30. Además, controla las inserciones que los empleados realizan sobre "ofertas", almacenando en la tabla "control" el nombre del usuario, la fecha y hora, cada vez que se ingresa un nuevo registro en la tabla "ofertas".

Creamos un desencadenador que se dispara una vez por cada registro ingresado en la tabla "ofertas":

Eliminamos las tres tablas:

```sql
drop table libros;
drop table ofertas;
drop table control;
```

Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un disparador que se dispare una vez por cada registro ingresado en "ofertas"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "insert" sobre "ofertas":

```sql
create or replace trigger tr_ingresar_ofertas
    before insert
    on ofertas
    for each row
begin
    insert into Control values(user,sysdate);
end tr_ingresar_ofertas;
 /
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_INGRESAR_OFERTAS';
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
```

Ingresamos en "ofertas" los libros de "libros" cuyo precio no superen los $30, utilizando la siguiente sentencia:

```sql
insert into ofertas select titulo,autor,precio from libros where precio<30;
```

Verificamos que el trigger se disparó 2 veces, una por cada fila afectada en la sentencia "insert" anteriormente ejecutada; consultamos la tabla "control":

```sql
select *from control;
```

Si el trigger hubiese sido creado a nivel de sentencia, no de fila, el "insert" anterior se hubiese activado una sola vez aun cuando se ingresaron 2 registros.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table ofertas;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_ingresar_ofertas
    before insert
    on ofertas
    for each row
begin
    insert into Control values(user,sysdate);
end tr_ingresar_ofertas;
 /
 
 select *from user_triggers where trigger_name ='TR_INGRESAR_OFERTAS';

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

insert into ofertas select titulo,autor,precio from libros where precio<30;

select *from control;
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en una tabla "control", el nombre del usuario y la fecha, cada vez que se ingresa un nuevo registro en la tabla "empleados".

1. Elimine las tablas:

```sql
drop table empleados;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    seccion varchar2(20)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

3. Cree un disparador que se dispare una vez por cada registro ingresado en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "insert" sobre "empleados"
```sql
create or replace trigger tr_empleados
after insert on empleados
for each row
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
4. Vea qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'EMPLEADOS';

```
5. Ingrese algunos registros en "empleados":

```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria');
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria');
insert into empleados values('22999000','FUENTES','Federico','Sistemas');
insert into empleados values('22555666','CASEROS','Carlos','Contaduria');
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas');
insert into empleados values('23666777','JUAREZ','Juan','Contaduria');
```

6. Verifique que el trigger se disparó 6 veces, una por cada fila afectada en la sentencia "insert" anteriormente ejecutada; consultamos la tabla "control":

```sql
select *from control;
```

Si el trigger hubiese sido creado a nivel de sentencia, no de fila, el "insert" anterior se hubiese activado una sola vez aún cuando se ingresaron 6 registros.
