# 27. Búsqueda de patrones (like - not like)

## Ejercicios de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    titulo varchar2(40),
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values('El aleph','Borges','Emece','12/05/2005',15.90);

insert into libros
values('Antología poética','J. L. Borges','Planeta','16/08/2000',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,'25/04/2000',19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI','21/12/2006',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,'22/09/2001',40);

insert into libros
values('Aprenda PHP','Mario Molina','Nuevo siglo','22/05/1999',56.50);

insert into libros
values(null,'Mario Molina','Nuevo siglo',null,45);
```

Recuperamos todos los libros que contengan en el campo "autor" la cadena "Borges":

```sql
select * from libros
where autor like '%Borges%';
```

Seleccionamos los libros cuyos títulos comienzan con la letra "M":

```sql
select * from libros
where titulo like 'M%';
```

Note que los valores nulos no se incluyen en la búsqueda.

Seleccionamos todos los títulos que NO comienzan con "M":

```sql
select * from libros
where titulo not like 'M%';
```

Si queremos ver los libros de "Lewis Carroll" pero no recordamos si se escribe "Carroll" o "Carrolt", podemos emplear el comodín "_" (guión bajo) y establecer la siguiente condición:

```sql
select * from libros
where autor like '%Carrol_';
```

Recuperamos todos los libros que contengan en el campo "edicion", en la parte correspondiente al mes, la cadena "05":

```sql
select titulo,edicion from libros
where edicion like '__/05%';
```

Note que especificamos que los 2 primeros caracteres (2 guiones bajos) pueden ser cualquier caracter, luego una barra seguida de "05" y que finalice con cualquier número de caracteres.

Recuperamos todos los libros cuyo precio se encuentra entre 10.00 y 19.99:

```sql
select titulo,precio from libros
where precio like '1_,%';
```

Note que especificamos que el segundo caracter (1 guión bajo) puede ser cualquier valor, luego una coma y que finalice con cualquier número de caracteres.

Recuperamos los libros que NO incluyen centavos en sus precios:

```sql
select titulo,precio from libros
where precio not like '%,%';
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece','12/05/2005',15.90);

insert into libros
values('Antología poética','J. L. Borges','Planeta','16/08/2000',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,'25/04/2000',19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI','21/12/2006',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,'22/09/2001',40);

insert into libros
values('Aprenda PHP','Mario Molina','Nuevo siglo','22/05/1999',56.50);

insert into libros
values(null,'Mario Molina','Nuevo siglo',null,45);

select * from libros
where autor like '%Borges%';

select * from libros
where titulo like 'M%';

select * from libros
where titulo not like 'M%';

select * from libros
where autor like '%Carrol_';

select titulo,edicion from libros
where edicion like '__/05%';

select titulo,precio from libros
where precio like '1_,%';

select titulo,precio from libros
where precio not like '%,%';
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla "empleados".

1. Elimine la tabla:

```sql
drop table empleados;
```

2. Cree la tabla:

```sql
create table empleados(
    nombre varchar2(30),
    documento char(8) not null,
    domicilio varchar2(30),
    fechaingreso date,
    seccion varchar2(20),
    sueldo number(6,2),
    primary key(documento)
);
```

3. Ingrese algunos registros:

```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','08/10/1990','Gerencia',900.50);

insert into empleados
values('Ana Acosta','23444555','Caseros 987','18/12/1995','Secretaria',590.30);

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','15/05/2005','Sistemas',790);

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','12/02/1999','Secretaria',550);

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','22/09/2002','Contaduria',630.70);

insert into empleados
values('Yolanda perez','35111222','Colon 180','08/10/1990','Administracion',400);

insert into empleados
values('Rodolfo perez','35555888','Coronel Olmedo 588','28/05/1990','Sistemas',800);
```

4. Muestre todos los empleados con apellido "Perez" empleando el operador "like" (1 registro)
Note que no incluye los "perez" (que comienzan con minúscula).
```sql
SELECT *
FROM empleados
WHERE nombre LIKE '% Perez';

```
5. Muestre todos los empleados cuyo domicilio comience con "Co" y tengan un "8" (2 registros)
```sql
SELECT *
FROM empleados
WHERE domicilio LIKE 'Co%' AND domicilio LIKE '%8%';

```
6. Seleccione todos los empleados cuyo documento finalice en 0 ó 4 (2 registros)
```sql
SELECT *
FROM empleados
WHERE documento LIKE '%0' OR documento LIKE '%4';

```
7. Seleccione todos los empleados cuyo documento NO comience con 2 y cuyo nombre finalice en "ez" (1 registro)
```sql
SELECT *
FROM empleados
WHERE documento NOT LIKE '2%' AND nombre LIKE '%ez';

```
8. Recupere todos los nombres que tengan una "G" o una "J" en su nombre o apellido (3 registros)
```sql
SELECT nombre
FROM empleados
WHERE nombre LIKE '%G%' OR nombre LIKE '%J%' OR nombre LIKE '%G%' OR nombre LIKE '%J%';

```
9. Muestre los nombres y sección de los empleados que pertenecen a secciones que comiencen con "S" o "G" y tengan 8 caracteres (3 registros)
```sql
SELECT nombre, seccion
FROM empleados
WHERE seccion LIKE '[SG]_______';

```
10. Muestre los nombres y sección de los empleados que pertenecen a secciones que NO comiencen con "S" (3 registros)
```sql
SELECT nombre, seccion
FROM empleados
WHERE seccion NOT LIKE 'S%';

```
11. Muestre todos los nombres y sueldos de los empleados cuyos sueldos se encuentren entre 500,00 y 599,99 empleando "like" (2 registros)
```sql
SELECT nombre, sueldo
FROM empleados
WHERE sueldo LIKE '5__.__';

```
12. Muestre los empleados que hayan ingresado en la década del 90 (5 registros)
```sql
SELECT *
FROM empleados
WHERE fechaingreso BETWEEN to_date('01/01/1990', 'dd/mm/yyyy') AND to_date('31/12/1999', 'dd/mm/yyyy');

```
13. Agregue 50 centavos a todos los sueldos que no tengan centavos (4 registros) y verifique recuperando todos los registros.
```sql
UPDATE empleados
SET sueldo = sueldo + 0.50
WHERE sueldo = ROUND(sueldo, 0);

SELECT *
FROM empleados;

```
14. Elimine todos los empleados cuyos apellidos comiencen con "p" (minúscula) (2 registros)
```sql
DELETE FROM empleados
WHERE nombre LIKE 'p%';

SELECT *
FROM empleados;

```