# ESTUDIANTE
## BRENDA ESTEFANY FLORES VELASQUEZ

[1. Crear tablas (create table - describe - all_tables - drop table)](consulta01.md)

[2. Ingresar registros (insert into- select)](consulta02.md)

[3. Tipos de datos](consulta03.md)

[4. Recuperar algunos campos (select)](consulta04.md)

[5. Recuperar algunos registros (where)](consulta05.md)

[6. Operadores relacionales](consulta06.md)

[7. Borrar registros (delete)](consulta07.md)

[8. Actualizar registros (update)](consulta08.md)

[10. Valores nulos (null)](consulta10.md)

[11. Operadores relacionales (is null)](consulta11.md)

[12. Clave primaria (primary key)](consulta12.md)

[13. Vaciar la tabla (truncate table)](consulta13.md)

[14. Tipos de datos alfanuméricos](consulta14.md)

[15. Tipos de datos numéricos](consulta15.md)

[16. Ingresar algunos campos](consulta16.md)

[17. Valores por defecto (default)](consulta17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](consulta18.md)

[19.  Alias (encabezados de columnas)](consulta19.md)

[20. Funciones string](consulta20.md)

[21. Funciones matemáticas](consulta21.md)

[22. Funciones de fechas y horas](consulta22.md)

[23. Ordenar registros (order by)](consulta23.md)

[24. Operadores lógicos (and - or - not)](consulta24.md)

[25. Otros operadores relacionales (between)](consulta25.md)

[26. Otros operadores relacionales (in)](consulta26.md)

[27. Búsqueda de patrones (like - not like)](consulta27.md)

[28. Contar registros (count)](consulta28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](consulta29.md)

[30. Agrupar registros (group by)](consulta30.md)

[31. Seleccionar grupos (Having)](consulta31.md)

[32. Registros duplicados (Distinct)](consulta32.md)

[33. Clave primaria compuesta](consulta33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](consulta34.md)

[35. Alterar secuencia (alter sequence)](consulta35.md)

[37. Restricción primary key](consulta37.md)

[38. Restricción unique](consulta38.md)

[39. Restriccioncheck](consulta39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](consulta40.md)

[42. Restricciones: eliminación (alter table - drop constraint)](consulta42.md)

[44. Indices (Crear . Información)](consulta44.md)

[45. Indices (eliminar)](consulta45.md)

[46. Varias tablas (join)](consulta46.md)

[47. Combinación interna (join)](consulta47.md)

[48. Combinación externa izquierda (left join)](consulta48.md)

[49. Combinación externa derecha (right join)](consulta49.md)

[50. Combinación externa completa (full join)](consulta50.md)

[51. Combinaciones cruzadas (cross)](consulta51.md)

[52. Autocombinación](consulta52.md)

[53. Combinaciones y funciones de agrupamiento](consulta53.md)

[54. Combinar más de 2 tablas](consulta54.md)

[55. Otros tipos de combinaciones](consulta55.md)

[57. Restricciones (foreign key)](consulta57.md)

[58. Restricciones foreign key en la misma tabla](consulta58.md)

[59. Restricciones foreign key (eliminación)](consulta59.md)

[60. Restricciones foreign key deshabilitar y validar](consulta60.md)

[61. Restricciones foreign key (acciones)](consulta61.md)

[63. Restricciones al crear la tabla](consulta63.md)

[64. Unión](consulta64.md)

[65. Intersección](consulta65.md)

[66. Minus](consulta66.md)

[67. Agregar campos (alter table-add)](consulta67.md)

[68. Modificar campos (alter table - modify)](consulta68.md)

[69. Eliminar campos (alter table - drop)](consulta69.md)

[70.  Agregar campos y restricciones (alter table)](consulta70.md)

[72. Subconsultas como expresion](consulta72.md)

[73. Subconsultas con in](consulta73.md)

[74. Subconsultas any- some - all](consulta74.md)

[75. Subconsultas correlacionadas](consulta75.md)

[76. Exists y No Exists](consulta76.md)

[77. Subconsulta simil autocombinacion](consulta77.md)

[78. Subconsulta conupdate y delete](consulta78.md)

[79. Subconsulta e insert](consulta/79.md)

[80. Crear tabla a partir de otra (create table-select)](consulta80.md)

[81. Vistas (create view)](consulta81.md)

[83. Vistas eliminar (drop view)](consulta83.md)

[84. Vistas (modificar datos a través de ella)](consulta84.md)

[85. Vistas (with read only)](consulta85.md)

[86. Vistas modificar (create or replace view)](consulta86.md)

[87. Vistas (with check option)](consulta87.md)

[88. Vistas (otras consideraciones: force)](consulta88.md)

[89. Vistas materializadas (materialized view)](consulta89.md)

[90. Procedimientos almacenados](consulta90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](consulta91.md)

[92. Procedimientos Almacenados (eliminar)](consulta92.md)

[93. Procedimientos almacenados (parámetros de entrada)](consulta93.md)

[94. Procedimientos almacenados (variables)](consulta94.md)

[95. Procedimientos Almacenados (informacion)](consulta95.md)

[96. Funciones](consulta96.md)

[97. Control de flujo (if)](consulta97.md)

[98. Control de flujo (case)](consulta98.md)

[99. Control de flujo (loop)](consulta99.md)

[100. Control de flujo (for)](consulta100.md)

[101. Control de flujo (while loop)](consulta101.md)

[102. Disparador (trigger)](consulta102.md)

[104. Disparador de inserción a nivel de sentencia](consulta104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](consulta105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](consulta106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](consulta107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](consulta108.md)

[109. Disparador de actualización - lista de campos (update trigger)](consulta109.md)

[110. Disparador de múltiples eventos](consulta110.md)