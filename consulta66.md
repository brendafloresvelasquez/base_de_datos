# 66. Minus

## Práctica de laboratorio

Una academia de enseñanza de idiomas da clases de inglés y frances; almacena los datos de los alumnos que estudian inglés en una tabla llamada "ingles" y los que están inscriptos en "francés" en una tabla denominada "frances".

Eliminamos las tablas:

```sql
drop table ingles;
drop table frances;
```

Creamos las tablas:

```sql
create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);
```

Ingresamos algunos registros:

```sql
insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');
```

La academia necesita el nombre y domicilio de todos los alumnos que cursan solamente inglés (no presentes en la tabla "frances") para enviarles publicidad referida al curso de francés. Empleamos el operador "minus" para obtener dicha información:

```sql
select nombre, domicilio from ingles
minus 
select nombre,domicilio from frances;
```

El resultado muestra los registros de la primer consulta que NO coinciden con ningún registro de la segunda consulta.

Los registros presentes en ambas tablas (Daniel Duarte y Estela Esper), no aparecen en el resultado final.

La academia necesita el nombre y domicilio de todos los alumnos que cursan solamente francés (no presentes en la tabla "ingles") para enviarles publicidad referida al curso de inglés. Empleamos el operador "minus" para obtener dicha información:

```sql
select nombre, domicilio from frances
minus 
select nombre,domicilio from ingles;
```

El resultado muestra los registros de la primer consulta que NO coinciden con ningún registro de la segunda consulta. Los registros presentes en ambas tablas (Daniel Duarte y Estela Esper), no aparecen en el resultado final.

Si queremos los alumnos que cursan un solo idioma (registros de "ingles" y de "frances" que no coinciden), podemos unir ambas tablas y luego restarle la intersección:

```sql
select nombre from ingles
union
select nombre from frances
  minus
 (select nombre from ingles
  intersect
select nombre from frances);
```

Podemos obtener el mismo resultado anterior con la siguiente consulta en la cual se buscan los registros de "ingles" que no coinciden con "frances" y los registros de "frances" que no coinciden con "ingles" y luego se unen ambos resultados:

```sql
select nombre from ingles
minus
select nombre from frances
union
(select nombre from frances
minus
select nombre from ingles);
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:
 
```sql
drop table ingles;
drop table frances;

create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');

select nombre, domicilio from ingles
minus 
select nombre,domicilio from frances;

select nombre, domicilio from frances
minus 
select nombre,domicilio from ingles;

select nombre from ingles
union
select nombre from frances
minus
(select nombre from ingles
intersect
select nombre from frances);

select nombre from ingles
minus
select nombre from frances
union
(select nombre from frances
minus
select nombre from ingles);
```

## Ejercicios propuestos

Una clínica almacena los datos de los médicos en una tabla llamada "medicos" y los datos de los pacientes en otra denominada "pacientes".

1. Eliminamos ambas tablas:

```sql
drop table medicos;
drop table pacientes;
```

2. Creamos las tablas:

```sql
create table medicos(
    legajo number(3),
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    especialidad varchar2(30),
    primary key(legajo)
);

create table pacientes(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    obrasocial varchar2(20),
    primary key(documento)
);
```

3. Ingresamos algunos registros:

```sql
insert into medicos values(1,'20111222','Ana Acosta','Avellaneda 111','clinica');
insert into medicos values(2,'21222333','Betina Bustos','Bulnes 222','clinica');
insert into medicos values(3,'22333444','Carlos Caseros','Colon 333','pediatria');
insert into medicos values(4,'23444555','Daniel Duarte','Duarte Quiros 444','oculista');
insert into medicos values(5,'24555666','Estela Esper','Esmeralda 555','alergia');
insert into pacientes values('24555666','Estela Esper','Esmeralda 555','IPAM');
insert into pacientes values('23444555','Daniel Duarte','Duarte Quiros 444','OSDOP');
insert into pacientes values('30111222','Fabiana Fuentes','Famatina 666','PAMI');
insert into pacientes values('30111222','Gaston Gonzalez','Guemes 777','PAMI');
```

4. La clínica necesita el nombre y domicilio de médicos y pacientes para enviarles una tarjeta de invitación a la inauguración de un nuevo establecimiento. Emplee el operador "union" para obtener dicha información de ambas tablas (7 registros)
```sql
SELECT nombre, domicilio
FROM medicos
UNION
SELECT nombre, domicilio
FROM pacientes;

```
5. Se necesitan los nombres de los médicos que también son pacientes de la clínica. Realice una intersección entre las tablas.
```sql
SELECT nombre
FROM medicos
INTERSECT
SELECT nombre
FROM pacientes;

```
6. La clínica necesita los nombres de los pacientes que no son médicos. Realice una operación de resta.
```sql
SELECT nombre
FROM pacientes
MINUS
SELECT nombre
FROM medicos;

```
7. Se necesitan los registros que no coinciden en ambas tablas. Realice la operación necesaria.
```sql
SELECT m.nombre AS nombre_medico, p.nombre AS nombre_paciente
FROM medicos m
FULL OUTER JOIN pacientes p ON m.nombre = p.nombre
WHERE m.nombre IS NULL OR p.nombre IS NULL;

```