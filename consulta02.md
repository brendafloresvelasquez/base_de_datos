# Ingresar registros (insert into- select)

## Ejercicio 1:
### Trabaje con la tabla "agenda" que almacena información de sus amigos.
#### 1. Elimine la tabla "agenda".

```sql
drop table agenda;

```
Salida del Script

```sh
Table AGENDA borrado.

```
#### 2. Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11).

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

```sh
Table AGENDA creado.

```
#### 3. Visualice las tablas existentes para verificar la creación de "agenda" (all_tables).

```sql
select * from all_tables;

```
Salida del Script

```sh
SYS	DBMS_SQLPATCH_FILES	SYSTEM
BRENDA	USUARIOS	SENATI
BRENDA	AGENDA	SENATI

```
#### 4. Visualice la estructura de la tabla "agenda" (describe).

```sql
describe agenda;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```
#### 5. Ingrese los siguientes registros:

```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


```
#### 6. Seleccione todos los registros de la tabla.

```sql
select * from agenda;

```
Salida del Script

```sh
Moreno	Alberto	Colon 123	4234567
Torres	Juan	Avellaneda 135	4458787

```
#### 7. Elimine la tabla "agenda".

```sql
drop table agenda;

```
Salida del Script

```sh
Table AGENDA borrado.

```
#### 8. Intente eliminar la tabla nuevamente (aparece un mensaje de error).

```sql
drop table agenda;

```
Salida del Script
Nos da un error de ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 1 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
## Ejercicio 02
### Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.
#### 1. Elimine la tabla "libros"

```sql
drop table libros;

```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe

```sh
Error que empieza en la línea: 1 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15).

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);


```
Salida del Script

```sh
Table LIBROS creado.

```
#### 3. Visualice las tablas existentes.

```sql
select * from all_tables;

```
Salida del Script

```sh
BRENDA	LIBROS	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

```
#### 4. Visualice la estructura de la tabla "libros" Muestra los campos y los tipos de datos de la tabla "libros".

```sql
describe libros;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 

```
#### 5. Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```
#### 6. Muestre todos los registros (select) de "libros".

```sql
select * from libros;

```
Salida del Script

```sh
El aleph	Borges	Planeta
Martin Fierro	Jose Hernandez	Emece
Aprenda PHP	Mario Molina	Emece

```