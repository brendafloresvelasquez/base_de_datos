# 61. Restricciones foreign key (acciones)

## Practica de laboratorio

Una librería almacena la información de sus libros para la venta en dos tablas, "libros" y "editoriales".

Eliminamos ambas tablas:

```sql
drop table libros;
drop table editoriales;
```

Creamos las tablas:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);
```

Ingresamos algunos registros en ambas tablas:

```sql
insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(1,'El aleph','Borges',1);
insert into libros values(2,'Martin Fierro','Jose Hernandez',2);
insert into libros values(3,'Aprenda PHP','Mario Molina',2);
insert into libros values(4,'El anillo del hechicero','Gaskin',3);
```

Establecemos una restricción "foreign key" para evitar que se ingrese en "libros" un código de editorial inexistente en "editoriales" con la opción "on cascade" para eliminaciones:

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete cascade;
```

Consultamos "user_constraints":

```sql
select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';
```

En la columna "delete_rule" de la restricción "foreign key" mostrará "cascade".

Si eliminamos una editorial, se borra tal editorial de "editoriales" y todos los registros de "libros" de dicha editorial:

```sql
 delete from editoriales where codigo=1;
```

Veamos si la eliminación se extendió a "libros":

```sql
select *from libros;
```

El libro "El aleph", de la editorial con código 1 se ha eliminado.

Eliminamos la restricción "foreign key" de "libros":

```sql
alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;
```

Establecemos una restricción "foreign key" sobre "codigoeditorial" de "libros" con la opción "set null" para eliminaciones:

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete set null;
```

Consultamos "user_constraints":

```sql
select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';
```

En la columna "delete_rule" de la restricción "foreign key" mostrará "set null".

Si eliminamos una editorial cuyo código está presente en "libros", se borra tal editorial de "editoriales" y todos los registros de "libros" de dicha editorial se setean con el valor "null":

```sql
delete from editoriales where codigo=2;
select *from libros;
```

Ahora, los libros "Martin Fierro" y "Aprenda PHP" tiene valor nulo en "codigoeditorial".

Eliminamos la restricción "foreign key" de "libros":

```sql
alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;
```

Establecemos una restricción "foreign key" sobre "codigoeditorial" de "libros" sin especificar opción para eliminaciones:

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);
```

Consultamos "user_constraints":

```sql
select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';
```

En la columna "delete_rule" de la restricción "foreign key" mostrará "no action".

Intentamos eliminar una editorial cuyo código esté presente en "libros":

```sql
delete from editoriales where codigo=3;
```

Un mensaje de error indica que la acción no se ha realizado porque existen registros coincidentes.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(1,'El aleph','Borges',1);
insert into libros values(2,'Martin Fierro','Jose Hernandez',2);
insert into libros values(3,'Aprenda PHP','Mario Molina',2);
insert into libros values(4,'El anillo del hechicero','Gaskin',3);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete cascade;

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=1;

select *from libros;

alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete set null;

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=2;
select *from libros;

alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=3;
```

## Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias":

```sql
drop table clientes;
drop table provincias;
```

2. Créelas con las siguientes estructuras:

```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)    );
```

3. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
```

4. Establezca una restricción "foreign key" especificando la acción "set null" para eliminaciones.
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo) ON DELETE SET NULL;

```
5. Elimine el registro con código 3, de "provincias" y consulte "clientes" para ver qué cambios ha realizado Oracle en los registros coincidentes
Todos los registros con "codigoprovincia" 3 han sido seteados a null.
```sql
DELETE FROM provincias WHERE codigo = 3;

SELECT * FROM clientes;

```
6. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
7. Intente modificar el registro con código 2, de "provincias"
```sql
UPDATE provincias SET codigo = 5 WHERE codigo = 2;

```
8. Elimine la restricción "foreign key" establecida sobre "clientes"
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_codigoprovincia;

```
9. Establezca una restricción "foreign key" sobre "codigoprovincia" de "clientes" especificando la acción "cascade" para eliminaciones
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo) ON DELETE CASCADE;

```
10. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre las restricciones "foreign key" de la tabla "clientes"
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
11. Elimine el registro con código 2, de "provincias"
```sql
DELETE FROM provincias WHERE codigo = 2;

```
12. Verifique que el cambio se realizó en cascada, es decir, que se eliminó en la tabla "provincias" y todos los clientes de la provincia eliminada
```sql
SELECT * FROM provincias;
SELECT * FROM clientes;

```
13. Elimine la restricción "foreign key"
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_codigoprovincia;

```
14. Establezca una restricción "foreign key" sin especificar acción para eliminaciones
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

```
15. Intente eliminar un registro de la tabla "provincias" cuyo código exista en "clientes"
```sql
DELETE FROM provincias WHERE codigo = 1;

```
16. Consulte el diccionario "user_constraints" para ver qué acción se ha establecido para las eliminaciones sobre la restricción "FK_CLIENTES_CODIGOPROVINCIA"
```sql
SELECT constraint_name, delete_rule
FROM user_constraints
WHERE constraint_name = 'FK_CODIGOPROVINCIA';

```
17. Intente elimimar la tabla "provincias"
```sql
DROP TABLE provincias;

```
18. Elimine la restricción "foreign key"
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_codigoprovincia;

```
19. Elimine la tabla "provincias"
```sql
DROP TABLE provincias;

```