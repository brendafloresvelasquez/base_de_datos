# 1. Crear tablas (create table - describe - all_tables - drop table)
## Práctica 01
```sql
create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

describe usuarios;

drop table usuarios;

select * from all_tables;

```
# EJERCICIOS
## Ejercicio 1:
### Necesita almacenar los datos de amigos en una tabla. Los datos que guardará serán: apellido, nombre, domicilio y teléfono.
#### 1. Elimine la tabla "agenda" Si no existe, un mensaje indicará tal situación.

```sql
drop table agenda;
```
Salida del Script

Nos muestra un error ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 1 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Intente crear una tabla llamada "*agenda".

```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

Nos muestra un error ORA-00903: nombre de tabla no válido
```sh

```
#### 3. Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11).

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

```sh
Table AGENDA creado.

```

#### 4. Intente crearla nuevamente.Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script
Nos muestra un error ORA-00955: este nombre ya lo está utilizando otro objeto existente.

#### 5. Visualice las tablas existentes (all_tables) La tabla "agenda" aparece en la lista.

```sql
select * from all_tables;

```
Salida del Script

```sh
BRENDA	AGENDA	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

```
#### 6. Visualice la estructura de la tabla "agenda" (describe) Aparece la siguiente tabla:

```sql
describe agenda;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```

## Ejercicio 2:
### Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.
#### 1. Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.

```sql
drop table libros;

```
Salida del Script
Nos da un error ORA-00942: la tabla o vista no existe.

#### 2. Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.

```sql
select * from all_tables;

```
Salida del Script 
Solo existe nuestra anterior tabla AGENDA.



#### 3. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15).

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);

```
Salida del Script

```sh
Table LIBROS creado.

```

#### 4. Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);

```
Salida del Script
Nos da un error ORA-00955: este nombre ya lo está utilizando otro objeto existente

```sh
Error que empieza en la línea: 5 del comando :
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
)
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:

```

#### 5. Visualice las tablas existentes.

```sql
select * from all_tables;

```
Salida del Script

```sh
BRENDA	AGENDA	SENATI
BRENDA	LIBROS	SENATI

```

#### 6. Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.

```sql
describe libros;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15)

```

#### 7. Elimine la tabla.

```sql
drop table libros;

```
Salida del Script

```sh
Table LIBROS borrado.

```

#### 8. Intente eliminar la tabla Un mensaje indica que no existe.

```sql
drop table libros;

```
Salida del Script
Nos da un error ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 15 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```



