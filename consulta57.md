# 57. Restricciones (foreign key)

## Practica de laboratorio

Una librería almacena la información de sus libros para la venta en dos tablas, "libros" y "editoriales".

Eliminamos ambas tablas:

```sql
drop table libros;
drop table editoriales;
```

Creamos las tablas:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);
```

Ingresamos algunos registros en ambas tablas:

```sql
insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',2);
insert into libros values(102,'Aprenda PHP','Mario Molina',5);
 ```

Intentamos establecer una restricción "foreign key" sobre "codigoeditorial":

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);
```

Mensaje de error; pues el campo "codigo" de la tabla "editoriales" no fue definida clave primaria ni única.

Agregamos una restricción "primary key" sobre "codigo" de "editoriales":

```sql
alter table editoriales
add constraint PK_editoriales
primary key (codigo);
```

Intentamos nuevamente establecer una restricción "foreign key" sobre "codigoeditorial":

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);
```

Mensaje de error. Oracle controla que los datos existentes no violen la restricción que intentamos establecer, como existe un valor de "codigoeditorial" inexistente en "editoriales", la restricción no puede establecerse.

Eliminamos el registro que infringe la regla:

```sql
delete from libros where codigoeditorial=5;
```

Ahora si podemos establecer una restricción "foreign key" sobre "codigoeditorial":

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);
```

Veamos las restricciones de "libros" consultando "user_constraints":

```sql
select constraint_name, constraint_type
from user_constraints
where table_name='LIBROS';
```

Aparece la restricción "FK_libros_codigoeditorial" indicando que es una "foreign key" con el caracter "R" en el tipo de restricción.

Consultamos "user_cons_columns":

```sql
select constraint_name, column_name
from user_cons_columns
where table_name='LIBROS';
```

Aparece la siguiente tabla:

```sh
CONSTRAINT_NAME              COLUMN_NAME
-------------------------------------------
FK_LIBROS_CODIGOEDITORIAL    CODIGOEDITORIAL
```

Veamos las restricciones de "editoriales":

```sql
select constraint_name, constraint_type
from user_constraints
where table_name='EDITORIALES';
```

Aparece la restricción "primary key".

Ingresamos un libro sin especificar un valor para el código de editorial:

```sql
insert into libros values(103,'El experto en laberintos','Gaskin',default);
```

Veamos todos los registros de "libros":

```sql
select *from libros;
```

Note que en "codigoeditorial" almacenó "null", porque esta restricción permite valores nulos (a menos que se haya especificado lo contrario al definir el campo).

Intentamos agregar un libro con un código de editorial inexistente en "editoriales":

```sql
insert into libros values(104,'El anillo del hechicero','Gaskin',8);
```

Nos muestra un mensaje indicando que la restricción FK_LIBROS_EDITORIAL está siendo violada, que no encuentra el valor de clave primaria en "editoriales".

Intentamos eliminar una editorial cuyo código esté presente en "libros":

```sql
delete from editoriales where codigo=2;
```

Un mensaje nos informa que la restricción de clave externa está siendo violada, existen registros que hacen referencia al que queremos eliminar.

Intente eliminar la tabla "editoriales":

```sql
drop table editoriales;
```

Un mensaje de error indica que la acción no puede realizarse porque la tabla es referenciada por una "foreign key".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',2);
insert into libros values(102,'Aprenda PHP','Mario Molina',5);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

alter table editoriales
add constraint PK_editoriales
primary key (codigo);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

delete from libros where codigoeditorial=5;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

select constraint_name, constraint_type
from user_constraints
where table_name='LIBROS';

select constraint_name, column_name
from user_cons_columns
where table_name='LIBROS';

select constraint_name, constraint_type
from user_constraints
where table_name='EDITORIALES';

insert into libros values(103,'El experto en laberintos','Gaskin',default);

select *from libros;

insert into libros values(104,'El anillo del hechicero','Gaskin',8);

delete from editoriales where codigo=2;

drop table editoriales;
```

## Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias" y créelas:

```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20)
);
```

En este ejemplo, el campo "codigoprovincia" de "clientes" es una clave foránea, se emplea para enlazar la tabla "clientes" con "provincias".

2. Intente agregar una restricción "foreign key" a la tabla "clientes" que haga referencia al campo "codigo" de "provincias"
No se puede porque "provincias" no tiene restricción "primary key" ni "unique".
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

```
3. Establezca una restricción "unique" al campo "codigo" de "provincias"
```sql
ALTER TABLE provincias ADD CONSTRAINT uk_codigo UNIQUE (codigo);

```
4. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6);
```

5. Intente agregar la restricción "foreign key" del punto 2 a la tabla "clientes"
No se puede porque hay un registro en "clientes" cuyo valor de "codigoprovincia" no existe en "provincias".
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

```
6. Elimine el registro de "clientes" que no cumple con la restricción y establezca la restricción nuevamente.
```sql
DELETE FROM clientes WHERE codigo = 6;
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

```
7. Intente agregar un cliente con un código de provincia inexistente en "provincias"
```sql
INSERT INTO clientes VALUES (104, 'Gonzalez Maria', 'San Martin 789', 'Córdoba', 5);

```
8. Intente eliminar el registro con código 3, de "provincias".
No se puede porque hay registros en "clientes" al cual hace referencia.
```sql
DELETE FROM provincias WHERE codigo = 3;

```
9. Elimine el registro con código "4" de "provincias"
Se permite porque en "clientes" ningún registro hace referencia a él.
```sql
DELETE FROM provincias WHERE codigo = 4;

```
10. Intente modificar el registro con código 1, de "provincias"
No se puede porque hay registros en "clientes" al cual hace referencia.
```sql
UPDATE provincias SET codigo = 5 WHERE codigo = 1;

```
11. Vea las restricciones de "clientes" consultando "user_constraints"
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
12. Vea las restricciones de "provincias"
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'PROVINCIAS';

```
13. Intente eliminar la tabla "provincias" (mensaje de error)
```sql
DROP TABLE provincias;

```
14. Elimine la restricción "foreign key" de "clientes" y luego elimine la tabla "provincias"
```sql
ALTER TABLE clientes DROP CONSTRAINT fk_codigoprovincia;
DROP TABLE provincias;

```