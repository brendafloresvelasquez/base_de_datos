# 104. Disparador de inserción a nivel de sentencia

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se ingresa un registro en la tabla "libros".

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla con la siguiente estructura:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);
```

Creamos la tabla "control", antes la eliminamos:

```sql
drop table control;
create table control(
    usuario varchar2(30),
    fecha date
);
```

Creamos un disparador que se dispare cada vez que se ingrese un registro en "libros"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "insert" sobre "libros":

```sql
create or replace trigger tr_ingresar_libros
    before insert
    on libros
begin
    insert into Control values(user,sysdate);
end tr_ingresar_libros;
 /
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_INGRESAR_LIBROS';
```

obtenemos la siguiente información:

- trigger_name: nombre del disparador;

- trigger_type: momento y nivel, en este caso es un desencadenador "before" y a nivel de sentencia (statement);

- triggering_event: evento que lo dispara, en este caso, "insert";

- base_object_type: a qué objeto está asociado, puede ser una tabla o una vista, en este caso, una tabla (table);

- table_name: nombre de la tabla al que está asociado (libros);

- y otras columnas que no analizaremos por el momento.

Ingresamos un registro en "libros":

```sql
insert into libros values(100, 'Uno', 'Richard Bach', 25);
```

Verificamos que el trigger se disparó consultando la tabla "control" para ver si tiene un nuevo registro:

```sql
select *from control;
```

Ingresamos dos registros más en "libros":

```sql
insert into libros values(150, 'Matematica estas ahi', 'Paenza', 12);
insert into libros values(185, 'El aleph', 'Borges', 42);
```

Verificamos que el trigger se disparó consultando la tabla "control" para ver si tiene dos nuevos registros:

```sql
select *from control;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

drop table control;
create table control(
    usuario varchar2(30),
    fecha date
);

 -- Creamos un disparador que se dispare cada vez que se ingrese un registro en "libros";
 -- el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha
 -- y la hora en la cual se realizó un "insert" sobre "libros":
create or replace trigger tr_ingresar_libros
    before insert
    on libros
begin
    insert into Control values(user,sysdate);
end tr_ingresar_libros;
 /
 
 -- Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

 -- Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:select *from user_triggers where trigger_name ='TR_INGRESAR_LIBROS';
insert into libros values(100,'Uno','Richard Bach',25);

 -- Verificamos que el trigger se disparó consultando la tabla "control"
 -- para ver si tiene un nuevo registro:select * from control;

 -- Ingresamos dos registros más en "libros":insert into libros values(150,'Matematica estas ahi','Paenza',12);insert into libros values(185,'El aleph','Borges',42);

 -- Verificamos que el trigger se disparó consultando la tabla "control" para ver si tiene
 -- dos nuevos registros:select * from control;
```

## Ejercicios propuestos

Una librería almacena los datos de sus libros en una tabla denominada "libros" y en una tabla "ofertas", algunos datos de los libros cuyo precio no supera los $30. Además, controla las inserciones que los empleados realizan sobre "ofertas", almacenando en la tabla "control" el nombre del usuario, la fecha y hora, cada vez que se ingresa un nuevo registro en la tabla "ofertas".

1. Elimine las tres tablas:

```sql
drop table libros;
drop table ofertas;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

3. Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set nls_date_format = 'DD/MM/YYYY HH24:MI';

```
4. Cree un disparador que se dispare cuando se ingrese un nuevo registro en "ofertas"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "insert" sobre "ofertas"

```sql
create or replace trigger tr_ofertas
after insert on ofertas
for each row
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
5. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado

```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'OFERTAS';

```
6. Ingrese algunos registros en "libros"

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
```

7. Ingrese en "ofertas" los libros de "libros" cuyo precio no superen los $30, utilizando la siguiente sentencia:

```sql
insert into ofertas select titulo,autor,precio from libros where precio<30;
```

8. Verifique que el trigger se disparó consultando la tabla "control".
```sql
select * from control;

```