# 93. Procedimientos almacenados (parámetros de entrada)

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla y la creamos nuevamente:

```sql
drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(5,2)
);
```

Ingresamos algunos registros:

```sql
insert into libros values ('Uno','Richard Bach','Planeta',15);
insert into libros values ('Ilusiones','Richard Bach','Planeta',12);
insert into libros values ('El aleph','Borges','Emece',25);
insert into libros values ('Aprenda PHP','Mario Molina','Nuevo siglo',50);
insert into libros values ('Matematica estas ahi','Paenza','Nuevo siglo',18);
insert into libros values ('Puente al infinito','Bach Richard','Sudamericana',14);
insert into libros values ('Antología','J. L. Borges','Paidos',24);
insert into libros values ('Java en 10 minutos','Mario Molina','Siglo XXI',45);
insert into libros values ('Cervantes y el quijote','Borges- Casares','Planeta',34);
```

Creamos un procedimiento que recibe el nombre de una editorial y luego aumenta en un 10% los precios de los libros de tal editorial:

```sql
create or replace procedure pa_libros_aumentar10(aeditorial in varchar2)
 as
 begin
  update libros set precio=precio+(precio*0.1)
  where editorial=aeditorial;
 end;
```

Ejecutamos el procedimiento:

```sql
execute pa_libros_aumentar10('Planeta');
```

Verificamos que los precios de los libros de la editorial "Planeta" han aumentado un 10%:

```sql
select *from libros;
```

Creamos otro procedimiento que recibe 2 parámetros, el nombre de una editorial y el valor de incremento (que tiene por defecto el valor 10):

```sql
create or replace procedure pa_libros_aumentar(aeditorial in varchar2,aporcentaje in number default 10)
 as
 begin
  update libros set precio=precio+(precio*aporcentaje/100)
  where editorial=aeditorial;
 end;
```

Ejecutamos el procedimiento enviando valores para ambos argumentos:

```sql
execute pa_libros_aumentar('Planeta',30);
```

Consultamos la tabla "libros" para verificar que los precios de los libros de la editorial "Planeta" han sido aumentados en un 30%:

```sql
select *from libros;
```

Ejecutamos el procedimiento "pa_libros_aumentar" omitiendo el segundo parámetro porque tiene establecido un valor por defecto:

```sql
execute pa_libros_aumentar('Paidos');
```

Consultamos la tabla "libros" para verificar que los precios de los libros de la editorial "Paidos" han sido aumentados en un 10% (valor por defecto):

```sql
select *from libros;
```

Definimos un procedimiento almacenado que ingrese un nuevo libro en la tabla "libros":

```sql
create or replace procedure pa_libros_insertar
  (atitulo in varchar2 default null, aautor in varchar2 default 'desconocido',
   aeditorial in varchar2 default 'sin especificar', aprecio in number default 0)
 as
 begin
 insert into libros values (atitulo,aautor,aeditorial,aprecio);
 end;
```

Llamamos al procedimiento sin enviarle valores para los parámetros:

```sql
execute pa_libros_insertar;
```

Veamos cómo se almacenó el registro:

```sql
select *from libros;
```

Llamamos al procedimiento enviándole valores solamente para el primer y cuarto parámetros correspondientes al título y precio:

```sql
execute pa_libros_insertar('Uno',100);
```

Oracle asume que los argumentos son el primero y segundo, vea cómo se almacenó el nuevo registro:

```sql
select *from libros;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
 drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(5,2)
);

insert into libros values ('Uno','Richard Bach','Planeta',15);
insert into libros values ('Ilusiones','Richard Bach','Planeta',12);
insert into libros values ('El aleph','Borges','Emece',25);
insert into libros values ('Aprenda PHP','Mario Molina','Nuevo siglo',50);
insert into libros values ('Matematica estas ahi','Paenza','Nuevo siglo',18);
insert into libros values ('Puente al infinito','Bach Richard','Sudamericana',14);
insert into libros values ('Antología','J. L. Borges','Paidos',24);
insert into libros values ('Java en 10 minutos','Mario Molina','Siglo XXI',45);
insert into libros values ('Cervantes y el quijote','Borges- Casares','Planeta',34);

 -- Creamos un procedimiento que recibe el nombre de una editorial y luego aumenta en
 -- un 10% los precios de los libros de tal editorial:
create or replace procedure pa_libros_aumentar10(aeditorial in varchar2)
 as
 begin
  update libros set precio=precio+(precio*0.1)
  where editorial=aeditorial;
 end;
 /
 
-- Ejecutamos el procedimiento:
execute pa_libros_aumentar10('Planeta');

 -- Verificamos que los precios de los libros de la editorial "Planeta" han aumentado un 10%:
select * from libros;

 -- Creamos otro procedimiento que recibe 2 parámetros, el nombre de una editorial
 -- y el valor de incremento (que tiene por defecto el valor 10):
create or replace procedure pa_libros_aumentar(aeditorial in varchar2,aporcentaje in number default 10)
 as
 begin
  update libros set precio=precio+(precio*aporcentaje/100)
  where editorial=aeditorial;
 end;
 /
 
 -- Ejecutamos el procedimiento enviando valores para ambos argumentos:
execute pa_libros_aumentar('Planeta',30);

 --Consultamos la tabla "libros" para verificar que los precios de los libros de la
 -- editorial "Planeta" han sido aumentados en un 30%:
select * from libros;

-- Ejecutamos el procedimiento "pa_libros_aumentar" omitiendo el segundo parámetro
-- porque tiene establecido un valor por defecto:
execute pa_libros_aumentar('Paidos');

 -- Consultamos la tabla "libros" para verificar que los precios de los libros
 -- de la editorial "Paidos" han sido aumentados en un 10% (valor por defecto):
select * from libros;

 --Definimos un procedimiento almacenado que ingrese un nuevo libro en la tabla "libros":
create or replace procedure pa_libros_insertar
  (atitulo in varchar2 default null, aautor in varchar2 default 'desconocido',
   aeditorial in varchar2 default 'sin especificar', aprecio in number default 0)
 as
 begin
 insert into libros values (atitulo,aautor,aeditorial,aprecio);
 end;
 /

 -- Llamamos al procedimiento sin enviarle valores para los parámetros:
execute pa_libros_insertar;

 -- Veamos cómo se almacenó el registro:
select *from libros;

 --Llamamos al procedimiento enviándole valores solamente para el primer
 -- y cuarto parámetros correspondientes al título y precio:

execute pa_libros_insertar('Uno',100);
 -- Oracle asume que los argumentos son el primero y segundo,
 -- vea cómo se almacenó el nuevo registro:
select * from libros;
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

1. Eliminamos la tabla y la creamos:

```sql
 drop table empleados;

create table empleados(
    documento char(8),
    nombre varchar2(20),
    apellido varchar2(20),
    sueldo number(6,2),
    fechaingreso date
);
```

2. Ingrese algunos registros:

```sql
insert into empleados values('22222222','Juan','Perez',300,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',300,'12/05/1998');
insert into empleados values('22444444','Marta','Perez',500,'25/08/1990');
insert into empleados values('22555555','Susana','Garcia',400,'05/05/2000');
insert into empleados values('22666666','Jose Maria','Morales',400,'24/10/2005');
```

3. Cree un procedimiento almacenado llamado "pa_empleados_aumentarsueldo". Debe incrementar el sueldo de los empleados con cierta cantidad de años en la empresa (parámetro "ayear" de tipo numérico) en un porcentaje (parámetro "aporcentaje" de tipo numerico); es decir, recibe 2 parámetros.
```sql
CREATE OR REPLACE PROCEDURE pa_empleados_aumentarsueldo(p_ayear IN NUMBER, p_aporcentaje IN NUMBER) IS
BEGIN
    UPDATE empleados
    SET sueldo = sueldo * (1 + (p_aporcentaje / 100))
    WHERE EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM fechaingreso) >= p_ayear;
END;

```
4. Ejecute el procedimiento creado anteriormente.
```sql
BEGIN
    pa_empleados_aumentarsueldo(10, 20);
END;

```
5. Verifique que los sueldos de los empleados con más de 10 años en la empresa han aumentado un 20%.
```sql
SELECT * FROM empleados;

```
6. Ejecute el procedimiento creado anteriormente enviando otros valores como parámetros (por ejemplo, 8 y 10).
```sql
BEGIN
    pa_empleados_aumentarsueldo(8, 10);
END;

```
7. Verifique que los sueldos de los empleados con más de 8 años en la empresa han aumentado un 10%.
```sql
SELECT * FROM empleados;

```
8. Ejecute el procedimiento almacenado "pa_empleados_aumentarsueldo" sin parámetros.
```sql
BEGIN
    pa_empleados_aumentarsueldo(NULL, NULL);
END;

```
9. Cree un procedimiento almacenado llamado "pa_empleados_ingresar" que ingrese un empleado en la tabla "empleados", debe recibir valor para el documento, el nombre, apellido y almacenar valores nulos en los campos "sueldo" y "fechaingreso"
```sql
CREATE OR REPLACE PROCEDURE pa_empleados_ingresar(p_documento IN CHAR DEFAULT NULL, p_nombre IN VARCHAR2 DEFAULT NULL, p_apellido IN VARCHAR2 DEFAULT NULL) IS
BEGIN
    INSERT INTO empleados(documento, nombre, apellido)
    VALUES (p_documento, p_nombre, p_apellido);
END;

```
10. Ejecute el procedimiento creado anteriormente y verifique si se ha ingresado en "empleados" un nuevo registro.
```sql
BEGIN
    pa_empleados_ingresar('22777777', 'Laura', 'Lopez');
END;

```
11. Reemplace el procedimiento almacenado llamado "pa_empleados_ingresar" para que ingrese un empleado en la tabla "empleados", debe recibir valor para el documento (con valor por defecto nulo) y fechaingreso (con la fecha actual como valor por defecto), los demás campos se llenan con valor nulo
```sql
CREATE OR REPLACE PROCEDURE pa_empleados_ingresar(
    p_documento IN CHAR DEFAULT NULL,
    p_nombre IN VARCHAR2 DEFAULT NULL,
    p_apellido IN VARCHAR2 DEFAULT NULL,
    p_fechaingreso IN DATE DEFAULT SYSDATE
) IS
BEGIN
    INSERT INTO empleados(documento, nombre, apellido, fechaingreso)
    VALUES (p_documento, p_nombre, p_apellido, p_fechaingreso);
END;

```
12. Ejecute el procedimiento creado anteriormente enviándole valores para los 2 parámetros y verifique si se ha ingresado en "empleados" un nuevo registro
```sql
BEGIN
    pa_empleados_ingresar('22888888', 'Pedro', 'Gomez', TO_DATE('01/06/2023', 'DD/MM/YYYY'));
END;

SELECT * FROM empleados;

```
13. Ejecute el procedimiento creado anteriormente enviando solamente la fecha de ingreso y vea el resultado
Oracle toma el valor enviado como primer argumento e intenta ingresarlo en el campo "documento", muestra un mensaje de error indicando que el valor es muy grande, ya que tal campo admite 8 caracteres.
```sql
BEGIN
    pa_empleados_ingresar(p_fechaingreso => TO_DATE('01/06/2023', 'DD/MM/YYYY'));
END;

```
14. Cree (o reemplace) un procedimiento almacenado que reciba un documento y elimine de la tabla "empleados" el empleado que coincida con dicho documento.
```sql
CREATE OR REPLACE PROCEDURE pa_empleados_eliminar(p_documento IN CHAR) IS
BEGIN
    DELETE FROM empleados WHERE documento = p_documento;
END;

```
15. Elimine un empleado empleando el procedimiento del punto anterior.
```sql
BEGIN
    pa_empleados_eliminar('22333333');
END;

```
16. Verifique la eliminación.
```sql
SELECT * FROM empleados;

```