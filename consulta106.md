# 106. Disparador de borrado (nivel de sentencia y de fila)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se elimina un registro de la tabla "libros".

Eliminamos la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;
```

Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(97,'Uno','Richard Bach','Planeta',25);
insert into libros values(98,'El aleph','Borges','Emece',28);
insert into libros values(99,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(100,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(101,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
insert into libros values(102,'El experto en laberintos','Gaskin','Planeta',20);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un disparador a nivel de fila, que se dispare cada vez que se borre un registro de "libros"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "delete" sobre "libros":

```sql
create or replace trigger tr_borrar_libros
    before delete
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';
```

obtenemos la siguiente información:

- trigger_name: nombre del disparador;

- trigger_type: momento y nivel, en este caso es un desencadenador "before" y a nivel de fila (each row);

- triggering_event: evento que lo dispara, en este caso, "delete";

- base_object_type: a qué objeto está asociado, puede ser una tabla o una vista, en este caso, una tabla (table);

- table_name: nombre de la tabla al que está asociado (libros);

- y otras columnas que no analizaremos por el momento.

Eliminamos todos los libros cuyo código sea inferior a 100:

```sql
delete from libros where codigo<100;
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Se eliminaron 3 registros, como el trigger fue definido a nivel de fila, se disparó 3 veces, una vez por cada registro eliminado. Si el trigger hubiese sido definido a nivel de sentencia, se hubiese disparado una sola vez.

Reemplazamos el disparador creado anteriormente por otro con igual código pero a nivel de sentencia:

```sql
create or replace trigger tr_borrar_libros
    before delete
    on libros
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';
```

en este caso es un desencadenador a nivel de sentencia.

Eliminamos todos los libros cuya editorial sea "Planeta":

```sql
delete from libros where editorial='Planeta';
```

Se han eliminado 2 registros, pero el trigger se ha disparado una sola vez, consultamos la tabla "control":

```sql
select *from control;
```

Si el trigger hubiese sido definido a nivel de fila, se hubiese disparado dos veces.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
  usuario varchar2(30),
  fecha date
);

insert into libros values(97,'Uno','Richard Bach','Planeta',25);
insert into libros values(98,'El aleph','Borges','Emece',28);
insert into libros values(99,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(100,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(101,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
insert into libros values(102,'El experto en laberintos','Gaskin','Planeta',20);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_borrar_libros
    before delete
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
 
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';

delete from libros where codigo<100;

select *from control;

create or replace trigger tr_borrar_libros
    before delete
    on libros
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
 
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';

delete from libros where editorial='Planeta';

select *from control;
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada empleado que se elimina de la tabla "empleados".

1. Elimine las tablas:

```sql
drop table empleados;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
  usuario varchar2(30),
  fecha date
);

3. Ingrese algunos registros en "empleados":

insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);
```

4. Cree un disparador a nivel de fila, que se dispare cada vez que se borre un registro de "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "delete" sobre "empleados"
```sql
create or replace trigger tr_empleados
after delete on empleados
for each row
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
5. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'EMPLEADOS';

```
6. Elimine todos los empleados cuyo sueldo supera los $800
```sql
delete from empleados where sueldo > 800;

```
7. Vea si el trigger se disparó consultando la tabla "control"
Se eliminaron 3 registros, como el trigger fue definido a nivel de fila, se disparó 3 veces, una vez por cada registro eliminado. Si el trigger hubiese sido definido a nivel de sentencia, se hubiese disparado una sola vez.
```sql
select * from control;

```
8. Reemplace el disparador creado anteriormente por otro con igual código pero a nivel de sentencia
```sql
create or replace trigger tr_empleados
before delete on empleados
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
9. Vea qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado
en este caso es un desencadenador a nivel de sentencia; en la columna "TRIGGER_TYPE" muestra "BEFORE STATEMENT".
```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'EMPLEADOS';

```
10. Elimine todos los empleados de la sección "Secretaria"
Se han eliminado 2 registros, pero el trigger se ha disparado una sola vez.
```sql
delete from empleados where seccion = 'Secretaria';

```
11. Consultamos la tabla "control"
Si el trigger hubiese sido definido a nivel de fila, se hubiese disparado dos veces.
```sql
select * from control;

```