# 11. Operadores relacionales (is null)
### Ejercicio 01

#### Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

##### 1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table medicamentos;
```
Salida del Script

```sh
Table MEDICAMENTOS borrado.
```
```sql
create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```
Salida del Script

```sh
Table MEDICAMENTOS creado.
```
##### 2. Visualice la estructura de la tabla "medicamentos"
```sql
DESCRIBE medicamentos;
```
Salida del Script

```sh
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)
```
note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".

##### 3. Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 4. Vea todos los registros.
```sql
SELECT * FROM medicamentos;
```
Salida del Script

```sh
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
3	Buscapina	Roche		200
```
##### 5. Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Aspirina','',0,200);
```
Salida del Script

```sh
1 fila insertadas.
```
##### 6. Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,'','',0,200);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."NOMBRE").
```sh
Error que empieza en la línea: 52 del comando -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,'','',0,200)
Error en la línea de comandos : 52 Columna : 79
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 7. Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Aspirina','',0,'');
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."CANTIDAD").
```sh
Error que empieza en la línea: 54 del comando -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Aspirina','',0,'')
Error en la línea de comandos : 54 Columna : 95
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."CANTIDAD")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 8. Recupere los registros que contengan valor "null" en el campo "laboratorio" (3 registros)
```sql
select *from medicamentos where laboratorio is null;
```
Salida del Script

```sh
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
4	Aspirina		0	200
```
##### 9. Recupere los registros que contengan valor "null" en el campo "precio", luego los que tengan el valor 0 en el mismo campo. Note que el resultado es distinto (2 y 1 registros respectivamente)
```sql
select *from medicamentos where precio is null;
```
Salida del Script

```sh
1	Sertal gotas			100
3	Buscapina	Roche		200
```
```sql
select *from medicamentos where precio=0;
```
Salida del Script

```sh
4	Aspirina		0	200
```
##### 10. Recupere los registros cuyo laboratorio no contenga valor nulo (1 registro)
```sql
select *from medicamentos where laboratorio is not null;
```
Salida del Script

```sh
3	Buscapina	Roche		200
```
##### 11. Recupere los registros cuyo precio sea distinto de 0, luego los que sean distintos de "null" (1 y 2 resgistros respectivamente)
```sql
select *from medicamentos where precio <>0;
```
Salida del Script

```sh
2	Sertal compuesto		8,9	150
```
```sql
select *from medicamentos where precio is not null;
```
Salida del Script

```sh
2	Sertal compuesto		8,9	150
4	Aspirina		0	200
```
Note que la salida de la primera sentencia no muestra los registros con valor 0 y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo precio (incluso el valor 0).

##### 12. Ingrese un registro con una cadena de 1 espacio para el laboratorio.
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(6,'Aspirina',' ',0,100);
```
Salida del Script

```sh
1 fila insertadas.
```
##### 13. Recupere los registros cuyo laboratorio sea "null" y luego los que contengan 1 espacio (3 y 1 registros respectivamente)
```sql
select *from medicamentos where laboratorio is null;
```
Salida del Script

```sh
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
4	Aspirina		0	200
```
```sql
select *from medicamentos where laboratorio=' ';
```
Salida del Script

```sh
6	Aspirina	 	0	100
```
Note que la salida de la primera sentencia no muestra los registros con valores para el campo "laboratorio" (un caracter espacio es un valor); el resultado de la segunda sentencia muestra los registros con el valor " " para el campo precio.

##### 14. Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio), luego los que sean distintos de "null" (1 y 2 registros respectivamente)
```sql
select *from medicamentos where laboratorio<>' ';
```
Salida del Script

```sh
3	Buscapina	Roche		200
```
```sql
select *from medicamentos where laboratorio is not null;
```
Salida del Script

```sh
3	Buscapina	Roche		200
6	Aspirina	 	0	100
```
Note que la salida de la primera sentencia no muestra los registros con valor ' ' y tampoco los que tienen valor nulo; el resultado de la segunda sentencia muestra los registros con valor para el campo laboratorio (incluso el valor ' ')

### Ejercicio 02

#### Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

##### 1. Elimine la tabla:

```sql
drop table peliculas;
```
Salida del Script

```sh
Table PELICULAS borrado.
```
##### 2. Créela con la siguiente estructura:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```
Salida del Script

```sh
Table PELICULAS creado.
```
##### 3. Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".
```sql
DESCRIBE peliculas;
```
Salida del Script

```sh
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3) 
```
##### 4. Ingrese los siguientes registros:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```
Salida de Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Recupere todos los registros para ver cómo Oracle los almacenó.
```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal		180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	0
```
##### 6. Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)
```sql
insert into peliculas (codigo,titulo,actor,duracion) values('','Mujer bonita','R. Gere.J. Roberts',0);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PELICULAS"."CODIGO").
```sh
Error que empieza en la línea: 94 del comando -
insert into peliculas (codigo,titulo,actor,duracion) values('','Mujer bonita','R. Gere.J. Roberts',0)
Error en la línea de comandos : 94 Columna : 61
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 7. Muestre los registros con valor nulo en el campo "actor" (2 registros)
```sql
select *from peliculas where actor is null;
```
Salida del Script

```sh
2	Harry Potter y la piedra filosofal		180
0	Mision imposible 2		150
```
##### 8. Actualice los registros que tengan valor de duración desconocido (nulo) por "120" (1 registro actualizado)
```sql
update peliculas set duracion=120 where duracion is null;
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 9. Coloque 'Desconocido' en el campo "actor" en los registros que tengan valor nulo en dicho campo (2 registros)
```sql
update peliculas set actor='Desconocido' where actor is null;
```
Salida del Script

```sh
2 filas actualizadas.
```
##### 10. Muestre todos los registros
```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal	Desconocido	180
3	Harry Potter y la camara secreta	Daniel R.	120
0	Mision imposible 2	Desconocido	150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	0
```
##### 11. Muestre todos los registros con valor nulo en el campo "actor" (ninguno)
```sql
select *from peliculas where actor is NULL;
```
Salida del Script

```sh
No hay ningun registro
```
##### 12. Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)
```sql
update peliculas set duracion=NULL where duracion =0;
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 13. Recupere todos los registros.
```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal	Desconocido	180
3	Harry Potter y la camara secreta	Daniel R.	120
0	Mision imposible 2	Desconocido	150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	
```
##### 14. Borre todos los registros en los cuales haya un valor nulo en "duracion" (1 registro)
```sql
delete from peliculas where duracion is NULL;
```
Salida del Script

```sh
1 fila eliminado
```
##### 15. Verifique que se eliminó recuperando todos los registros.
```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal	Desconocido	180
3	Harry Potter y la camara secreta	Daniel R.	120
0	Mision imposible 2	Desconocido	150
4	Titanic	L. Di Caprio	220
```
