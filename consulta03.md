# 3. Tipos de datos

## Ejercicio 01
### Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

```sh
 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).

```
#### 1. Elimine la tabla "peliculas" si ya existe.

```sql
drop table peliculas;

```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 1 del comando :
drop table peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

```sql
CREATE TABLE peliculas (
    nombre VARCHAR2(20),
    actor VARCHAR2(20),
    duracion NUMBER(3),
    cantidad NUMBER(1)
);


```
Salida del Script

```sh
Table PELICULAS creado.

```
#### 3. Vea la estructura de la tabla.

```sql
DESCRIBE peliculas;

```
Salida del Script

```sh
Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
NOMBRE          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(3)    
CANTIDAD        NUMBER(1)    

```
#### 4. Ingrese los siguientes registros: 

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);


```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
#### 5. Muestre todos los registros (4 registros).

```sql
select * from peliculas;

```
Salida del Script

```sh
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2

```
#### 6. Intente ingresar una película con valor de cantidad fuera del rango permitido:

```sql
 insert into peliculas (nombre, actor, duracion, cantidad)values ('Mujer bonita','Richard Gere',1200,10);

```
Salida del Script

Nos da un error de ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
```sh
Error que empieza en la línea: 19 del comando -
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10)
Error en la línea de comandos : 20 Columna : 41
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

```
#### 7. Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);

```
Salida del Script

```sh
1 fila insertadas.

```
#### 8. Muestre todos los registros para ver cómo se almacenó el último registro ingresado.

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);

```
Salida del Script

```sh
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2
Mujer bonita	Richard Gere	120	4

```

## Ejercicio 02
### Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico. 
#### 1. Elimine la tabla si existe.

```sql
DROP TABLE empleados;
```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 1 del comando :
DROP TABLE empleados
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo: 


```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```
Salida del Script


```sh
Table EMPLEADOS creado.

```
#### 3. Verifique que la tabla existe consultando
```sql
select *from all_tables;

```
Salida del Script

```sh
VARGAS	EMPLEADOS	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED
```
#### 4. Vea la estructura de la tabla (5 campos).

```sql
describe empleados;
```
Salida del Script

```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        NUMBER(6,2)  

```
#### 5. Ingrese algunos registros:

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);
```
Salida de Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
#### 6. Seleccione todos los registros (3 registros).

```sql 
select * from empleados;

```
Salida de Script 

```sh
Juan Perez	22333444	m	Sarmiento 123	500
Ana Acosta	24555666	f	Colon 134	650
Bartolome Barrios	27888999	m	Urquiza 479	800

```
#### 7. Intente ingresar un registro con el valor "masculino" en el campo "sexo". Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','masculino','Urquiza 479',800);

```
Salida de Script 

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "ZAPANA"."EMPLEADOS"."SEXO" (real: 9, máximo: 1)

```sh
Error que empieza en la línea: 21 del comando -
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','masculino','Urquiza 479',800)
Error en la línea de comandos : 21 Columna : 105
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "ZAPANA"."EMPLEADOS"."SEXO" (real: 9, máximo: 1)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).


```
#### 8. Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','m','Urquiza 479',200000000000000000);

```
Salida de Script 

Nos da un error de ORA-01438: valor mayor que el que permite la precisión especificada para esta columna.

```sh
Error que empieza en la línea: 21 del comando -
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','m','Urquiza 479',200000000000000000)
Error en la línea de comandos : 21 Columna : 123
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

```
#### 9. Elimine la tabla.

```sql 
DROP TABLE empleados;

```
Salida de Script 

```sh
Table EMPLEADOS borrado.

```