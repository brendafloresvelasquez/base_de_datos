# 97. Control de flujo (if)

## Practica de laboratorio

Un profesor almacena las notas de sus alumnos en una tabla denominada "notas".

Eliminamos la tabla:

```sql
drop table notas;
```

Creamos la tabla con la siguiente estructura:

```sql
create table notas(
    nombre varchar2(30),
    nota number(4,2)
);
```

Ingresamos algunos registros:

```sql
insert into notas values('Acosta Ana', 6.7);
insert into notas values('Bustos Brenda', 9.5);
insert into notas values('Caseros Carlos', 3.7);
insert into notas values('Dominguez Daniel', 2);
insert into notas values('Fuentes Federico', 8);
insert into notas values('Gonzalez Gaston', 7);
insert into notas values('Juarez Juana', 4);
insert into notas values('Lopez Luisa',5.3);
```

Creamos o reemplazamos la función "f_condicion" que recibe una nota y retorna una cadena de caracteres indicando si aprueba o no:

```sql
create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
    condicion:='desaprobado';
   else condicion:='aprobado';
   end if;
   return condicion;
 end;
 /
```

Realizamos un "select" sobre "notas" mostrando el nombre y nota del alumno y en una columna su condición (empleando la función creada anteriormente):

```sql
select nombre, nota, f_condicion(nota) from notas;
```

En el siguiente ejemplo omitimos la cláusula "else" porque sólo indicaremos acciones en caso que el "if" sea verdadero:

```sql
create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='aprobado';
   if anota<4 then
    condicion:='desaprobado';
   end if;
   return condicion;
 end;
 /
```

Realizamos el "select" sobre "notas" mostrando la misma información que antes:

```sql
select nombre, nota, f_condicion(nota) from notas;
```

En el siguiente ejemplo colocamos un "if" dentro de otro "if". En el cuerpo de la función controlamos si la nota es menor a 4 (retorna "desaprobado"), luego, dentro del "else", controlamos si la nota es menor a 8 (retorna "regular") y si no lo es ("else"), retorna "promocionado":

```sql
create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
     condicion:='desaprobado';
   else
     if anota<8 then
      condicion:='regular';
     else
      condicion:='promocionado';
     end if;
   end if;
   return condicion;
 end;
 /
```

Realizamos el "select" sobre "notas" mostrando la misma información que antes:

```sql
select nombre, nota, f_condicion(nota) from notas;
```

Simplificamos la función anteriormente creada empleando la sintaxis "if...elsif":

```sql
create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
     condicion:='desaprobado';
   elsif anota<8 then
     condicion:='regular';
   else
    condicion:='promocionado';
   end if;
   return condicion;
 end;
 /
```

Realizamos el "select" sobre "notas" mostrando la misma información que antes:

```sql
select nombre, nota, f_condicion(nota) from notas;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table notas;

 create table notas(
  nombre varchar2(30),
  nota number(4,2)
 );

insert into notas values('Acosta Ana', 6.7);
insert into notas values('Bustos Brenda', 9.5);
insert into notas values('Caseros Carlos', 3.7);
insert into notas values('Dominguez Daniel', 2);
insert into notas values('Fuentes Federico', 8);
insert into notas values('Gonzalez Gaston', 7);
insert into notas values('Juarez Juana', 4);
insert into notas values('Lopez Luisa',5.3);

 -- Creamos o reemplazamos la función "f_condicion" que recibe
 -- una nota y retorna una cadena de caracteres indicando si aprueba o no:
 create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
    condicion:='desaprobado';
   else condicion:='aprobado';
   end if;
   return condicion;
 end;
 /

 --Realizamos un "select" sobre "notas" mostrando el nombre y nota del
 -- alumno y en una columna su condición (empleando la función creada anteriormente):
select nombre, nota, f_condicion(nota) from notas;

 -- En el siguiente ejemplo omitimos la cláusula "else" porque sólo indicaremos
 -- acciones en caso que el "if" sea verdadero:
 create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='aprobado';
   if anota<4 then
    condicion:='desaprobado';
   end if;
   return condicion;
 end;
 /
 
 --Realizamos el "select" sobre "notas" mostrando la misma información que antes:
select nombre, nota, f_condicion(nota) from notas;

 -- En el siguiente ejemplo colocamos un "if" dentro de otro "if".
 create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
     condicion:='desaprobado';
   else
     if anota<8 then
      condicion:='regular';
     else
      condicion:='promocionado';
     end if;
   end if;
   return condicion;
 end;
 /
 
 --Realizamos el "select" sobre "notas" mostrando la misma información que antes:
select nombre, nota, f_condicion(nota) from notas;

 --Simplificamos la función anteriormente creada empleando la sintaxis "if...elsif":
 create or replace function f_condicion (anota number)
  return varchar2
 is
  condicion varchar2(20);
 begin
   condicion:='';
   if anota<4 then
     condicion:='desaprobado';
   elsif anota<8 then
     condicion:='regular';
   else
    condicion:='promocionado';
   end if;
   return condicion;
 end;
 /
 
 -- Realizamos el "select" sobre "notas" mostrando la misma información que antes:
select nombre, nota, f_condicion(nota) from notas;
```

## Ejercicios propuestos

Un negocio almacena los datos de sus productos en una tabla denominada "productos". Dicha tabla contiene el código de producto, el precio, el stock mínimo que se necesita (cantidad mínima requerida antes de comprar más) y el stock actual (cantidad disponible en depósito). Si el stock actual es cero, es urgente reponer tal producto; menor al stock mínimo requerido, es necesario reponer tal producto; si el stock actual es igual o supera el stock minimo, está en estado normal.

1. Elimine la tabla "productos":

```sql
drop table productos;
```

2. Cree la tabla con la siguiente estructura:

```sql
 create table productos(
  codigo number(5),
  precio number(6,2),
  stockminimo number(4),
  stockactual number(4)
 );
```

3. Ingrese algunos registros:

```sql
insert into productos values(100,10,100,200);
insert into productos values(102,15,200,500);
insert into productos values(130,8,100,0);
insert into productos values(240,23,100,20);
insert into productos values(356,12,100,250);
insert into productos values(360,7,100,100);
insert into productos values(400,18,150,100);
```

4. Cree una función que reciba dos valores numéricos correspondientes a ambos stosks. Debe comparar ambos stocks y retornar una cadena de caracteres indicando el estado de cada producto, si stock actual es:

```sql
CREATE OR REPLACE FUNCTION obtener_estado_producto(p_stockminimo IN NUMBER, p_stockactual IN NUMBER) RETURN VARCHAR2 IS
BEGIN
  IF p_stockactual = 0 THEN
    RETURN 'faltante';
  ELSIF p_stockactual < p_stockminimo THEN
    RETURN 'reponer';
  ELSE
    RETURN 'normal';
  END IF;
END;
/

```
 - cero: "faltante",
 - menor al stock mínimo: "reponer",
 - igual o superior al stock mínimo: "normal".
5. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto

```sql
SELECT codigo, stockactual, stockminimo, obtener_estado_producto(stockminimo, stockactual) AS estado_producto FROM productos;

```
6. Realice la misma función que en el punto 4, pero esta vez empleando en la estructura condicional la sintaxis "if... elsif...end if"

```sql
CREATE OR REPLACE FUNCTION obtener_estado_producto_cond(p_stockminimo IN NUMBER, p_stockactual IN NUMBER) RETURN VARCHAR2 IS
BEGIN
  IF p_stockactual = 0 THEN
    RETURN 'faltante';
  ELSIF p_stockactual < p_stockminimo THEN
    RETURN 'reponer';
  ELSE
    RETURN 'normal';
  END IF;
END;
/

```
7. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto

```sql
SELECT codigo, stockactual, stockminimo, obtener_estado_producto_cond(stockminimo, stockactual) AS estado_producto FROM productos;

```
8. Realice una función similar a las anteriores, pero esta vez, si el estado es "reponer" o "faltante", debe especificar la cantidad necesaria (valor necesario para llegar al stock mínimo)

```sql
CREATE OR REPLACE FUNCTION obtener_estado_producto_cant(p_stockminimo IN NUMBER, p_stockactual IN NUMBER) RETURN VARCHAR2 IS
  v_cantidad_necesaria NUMBER(4);
BEGIN
  IF p_stockactual = 0 THEN
    RETURN 'faltante';
  ELSIF p_stockactual < p_stockminimo THEN
    v_cantidad_necesaria := p_stockminimo - p_stockactual;
    RETURN 'reponer (' || v_cantidad_necesaria || ')';
  ELSE
    RETURN 'normal';
  END IF;
END;
/

```
9. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto
```sql
SELECT codigo, stockactual, stockminimo, obtener_estado_producto_cant(stockminimo, stockactual) AS estado_producto FROM productos;

```