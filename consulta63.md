# 63. Restricciones al crear la tabla

## Practica de laboratorio

Trabajamos con las tablas "libros", "autores" y "editoriales" de una librería:

Eliminamos las tablas:

```sql
drop table libros;
drop table editoriales;
drop table autores;
```

Recuerde eliminar en primer lugar "libros", pues si las tablas existen y "libros" hace referencia con una restricción "foreign key" a "editoriales" y "autores", tales tablas no podrán eliminarse hasta que ninguna restricción las referencie.

Creamos la tabla "editoriales" con una restricción "primary key":

```sql
create table editoriales(
    codigo number(3) not null,
    nombre varchar2(30),
    constraint PK_editoriales
    primary key (codigo)
);
```

Creamos la tabla "autores" con una restricción "primary key", una "unique" y una "check":

```sql
create table autores(
    codigo number(4) not null
    constraint CK_autores_codigo
    check (codigo>=0),
    nombre varchar2(30) not null,
    constraint PK_autores_codigo
    primary key (codigo),
    constraint UQ_autores_nombre
    unique (nombre)
);
```

Aplicamos varias restricciones cuando creamos la tabla "libros":

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    codigoautor number(4),
    codigoeditorial number(3),
    precio number(5,2) default 0,
    constraint PK_libros_codigo
    primary key (codigo),
    constraint UQ_libros_tituloautor
    unique (titulo,codigoautor),
    constraint CK_libros_codigoeditorial
    check (codigoeditorial is not null),
    constraint FK_libros_editorial
    foreign key (codigoeditorial)
    references editoriales(codigo)
    on delete cascade,
    constraint FK_libros_autores
    foreign key (codigoautor)
    references autores(codigo)
    on delete set null,
    constraint CK_libros_preciononulo
    check (precio is not null) disable,
    constraint CK_precio_positivo
    check (precio>=0)
);
```

Recuerde que si definimos una restricción "foreign key" al crear una tabla, la tabla referenciada debe existir, por ello creamos las tablas "editoriales" y "autores" antes que "libros".

Veamos las restricciones de "editoriales":

```sql
select constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='EDITORIALES';
```

Una tabla nos informa que hay una restricción de control y una "primary key", ambas habilitadas y validan los datos existentes.

Veamos las restricciones de "autores":

```sql
select constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='AUTORES';
```

Oracle nos informa que hay 3 restricciones de control, una "primary key" y una única.

Veamos las restricciones de "libros":

```sql
select constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='LIBROS';
```

la tabla tiene 7 restricciones: 3 de control (2 "enabled" y "validated" y 1 "disabled" y "not validated"), 1 "primary key" ("enabled" "validated"), 1 "unique" ("enabled" "validated") y 2 "foreign key" ("enabled" "validated").

Ingresamos algunos registros en las tres tablas.

Recuerde que debemos ingresar registros en las tablas "autores" y "editoriales" antes que en "libros", a menos que deshabilitemos las restricciones "foreign key".

```sql
insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Norma');
insert into autores values(100,'Borges');
insert into autores values(101,'Bach Richard');
insert into autores values(102,'Cervantes');
insert into autores values(103,'Gaskin');
insert into libros values(200,'El aleph',100,1,40);
insert into libros values(300,'Uno',101,2,20);
insert into libros values(400,'El quijote',102,3,20);
insert into libros values(500,'El experto en laberintos',103,3,null);
```

Note que un libro tiene precio nulo, la tabla "libros" tiene una restricción de control que no admite precios nulos, pero está deshabilitada.

Realizamos un "join" para mostrar todos los datos de los libros:

```sql
select l.codigo,a.nombre as autor,e.nombre as editorial,precio
from libros l
join autores a
on codigoautor=a.codigo
join editoriales e
on codigoeditorial=e.codigo;
```

Habilitamos la restricción de control deshabilitada sin controlar los datos ya cargados:

```sql
alter table libros
enable novalidate
constraint CK_LIBROS_PRECIONONULO;
```

Intentamos ingresar un libro con precio nulo:

```sql
insert into libros values(600,'El anillo del hechicero',103,3,null);
```

Oracle no lo permite, la restricción está habilitada.

Eliminamos un autor:

```sql
delete autores where codigo=100;
```

Veamos si se setearon a "null" los libros de tal autor (la restricción "FK_LIBROS_AUTORES" así lo especifica):

```sql
select *from libros;
```

El libro con código 200 tiene el valor "null" en "autor".

Eliminamos una editorial:

```sql
delete editoriales where codigo=1;
```

Veamos si se eliminaron los libros de tal editorial (la restricción "FK_LIBROS_EDITORIALES" fue establecida "cascade"):

```sql
insert *from libros;
```

Ya no está el libro "200".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;
drop table autores;

create table editoriales(
    codigo number(3) not null,
    nombre varchar2(30),
    constraint PK_editoriales
    primary key (codigo)
);

create table autores(
    codigo number(4) not null
    constraint CK_autores_codigo
    check (codigo>=0),
    nombre varchar2(30) not null,
    constraint PK_autores_codigo
    primary key (codigo),
    constraint UQ_autores_nombre
    unique (nombre)
);

create table libros(
    codigo number(5),
    titulo varchar2(40),
    codigoautor number(4),
    codigoeditorial number(3),
    precio number(5,2) default 0,
    constraint PK_libros_codigo
    primary key (codigo),
    constraint UQ_libros_tituloautor
    unique (titulo,codigoautor),
    constraint CK_libros_codigoeditorial
    check (codigoeditorial is not null),
    constraint FK_libros_editorial
    foreign key (codigoeditorial)
    references editoriales(codigo)
    on delete cascade,
    constraint FK_libros_autores
    foreign key (codigoautor)
    references autores(codigo)
    on delete set null,
    constraint CK_libros_preciononulo
    check (precio is not null) disable,
    constraint CK_precio_positivo
    check (precio>=0)
);

insert constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='EDITORIALES';

insert constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='AUTORES';

insert constraint_name, constraint_type, search_condition, status, validated
from user_constraints
where table_name='LIBROS';

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Norma');
insert into autores values(100,'Borges');
insert into autores values(101,'Bach Richard');
insert into autores values(102,'Cervantes');
insert into autores values(103,'Gaskin');
insert into libros values(200,'El aleph',100,1,40);
insert into libros values(300,'Uno',101,2,20);
insert into libros values(400,'El quijote',102,3,20);
insert into libros values(500,'El experto en laberintos',103,3,null);

insert l.codigo,a.nombre as autor,e.nombre as editorial,precio
from libros l
join autores a
on codigoautor=a.codigo
join editoriales e
on codigoeditorial=e.codigo;

alter table libros
enable novalidate
constraint CK_LIBROS_PRECIONONULO;

insert into libros values(600,'El anillo del hechicero',103,3,null);

delete autores where codigo=100;

insert *from libros;

delete editoriales where codigo=1;

insert *from libros;
```

 ## Ejercicios propuestos

 Un club de barrio tiene en su sistema 4 tablas:

- "socios": en la cual almacena documento, número, nombre y domicilio de cada socio;

- "deportes": que guarda un código, nombre del deporte, día de la semana que se dicta y documento del profesor instructor;

- "profesores": donde se guarda el documento, nombre y domicilio de los profesores e

- "inscriptos": que almacena el número de socio, el código de deporte y si la matricula está paga o no.

1. Elimine las tablas:

```sql
drop table inscriptos;
drop table socios;
drop table deportes;
drop table profesores;
```

2. Considere que:

- un socio puede inscribirse en varios deportes, pero no dos veces en el mismo.
- un socio tiene un documento único y un número de socio único.
- un deporte debe tener asignado un profesor que exista en "profesores" o "null" si aún no tiene un instructor definido.
- el campo "dia" de "deportes" puede ser: lunes, martes, miercoles, jueves, viernes o sabado.
- el campo "dia" de "deportes" por defecto debe almacenar 'sabado'.
- un profesor puede ser instructor de varios deportes o puede no dictar ningún deporte.
- un profesor no puede estar repetido en "profesores".
- un inscripto debe ser socio, un socio puede no estar inscripto en ningún deporte.
- una inscripción debe tener un valor en socio existente en "socios" y un deporte que exista en "deportes".
- el campo "matricula" de "inscriptos" debe aceptar solamente los caracteres 's' o'n'.
- si se elimina un profesor de "profesores", el "documentoprofesor" coincidente en "deportes" debe quedar seteado a null.
- no se puede eliminar un deporte de "deportes" si existen inscriptos para tal deporte en "inscriptos".
- si se elimina un socio de "socios", el registro con "numerosocio" coincidente en "inscriptos" debe eliminarse.

3. Cree las tablas con las restricciones necesarias:

```sql
create table profesores(
    documento char(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_profesores_documento
    primary key (documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20) not null,
    dia varchar2(9) default 'sabado',
    documentoprofesor char(8),
    constraint CK_deportes_dia_lista
    check (dia in ('lunes','martes','miercoles','jueves','viernes','sabado')),
    constraint PK_deportes_codigo
    primary key (codigo),
    constraint FK_deportes_profesor
    foreign key (documentoprofesor)
    references profesores(documento)
    on delete set null
);

create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_socios_numero
    primary key (numero),
    constraint UQ_socios_documento
    unique (documento)
);

create table inscriptos(
    numerosocio number(4),
    codigodeporte number(2),
    matricula char(1),
    constraint PK_inscriptos_numerodeporte
    primary key (numerosocio,codigodeporte),
    constraint FK_inscriptos_deporte
    foreign key (codigodeporte)
    references deportes(codigo),
    constraint FK_inscriptos_socios
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade,
    constraint CK_matricula_valores
    check (matricula in ('s','n'))
);
```

4. Ingrese registros en "profesores":

```sql
insert into profesores values('21111111','Andres Acosta','Avellaneda 111');
insert into profesores values('22222222','Betina Bustos','Bulnes 222');
insert into profesores values('23333333','Carlos Caseros','Colon 333');
```

5. Ingrese registros en "deportes". Ingrese el mismo día para distintos deportes, un deporte sin día confirmado, un deporte sin profesor definido:

```sql
insert into deportes values(1,'basquet','lunes',null);
insert into deportes values(2,'futbol','lunes','23333333');
insert into deportes values(3,'natacion',null,'22222222');
insert into deportes values(4,'padle',default,'23333333');
insert into deportes values(5,'tenis','jueves',null);
```

6. Ingrese registros en "socios":

```sql
insert into socios values(100,'30111111','Martina Moreno','America 111');
insert into socios values(200,'30222222','Natalia Norte','Bolivia 222');
insert into socios values(300,'30333333','Oscar Oviedo','Caseros 333');
insert into socios values(400,'30444444','Pedro Perez','Dinamarca 444');
```

7. Ingrese registros en "inscriptos". Inscriba a un socio en distintos deportes, inscriba varios socios en el mismo deporte.

```sql
insert into inscriptos values(100,3,'s');
insert into inscriptos values(100,5,'s');
insert into inscriptos values(200,1,'s');
insert into inscriptos values(400,1,'n');
insert into inscriptos values(400,4,'s');
```

8. Realice un "join" (del tipo que sea necesario) para mostrar todos los datos del socio junto con el nombre de los deportes en los cuales está inscripto, el día que tiene que asistir y el nombre del profesor que lo instruirá (5 registros)

```sql
select s.*, d.nombre as deporte, d.dia, p.nombre as profesor
from socios s
left join inscriptos i on s.numero = i.numerosocio
left join deportes d on i.codigodeporte = d.codigo
left join profesores p on d.documentoprofesor = p.documento;

```
9. Realice la misma consulta anterior pero incluya los socios que no están inscriptos en ningún deporte (6 registros)

```sql
select s.*, d.nombre as deporte, d.dia, p.nombre as profesor
from socios s
left join inscriptos i on s.numero = i.numerosocio
left join deportes d on i.codigodeporte = d.codigo
left join profesores p on d.documentoprofesor = p.documento;

```
10. Muestre todos los datos de los profesores, incluido el deporte que dicta y el día, incluyendo los profesores que no tienen asignado ningún deporte, ordenados por documento (4 registros)

```sql
select p.*, d.nombre as deporte, d.dia
from profesores p
left join deportes d on p.documento = d.documentoprofesor
order by p.documento;

```
11. Muestre todos los deportes y la cantidad de inscriptos en cada uno de ellos, incluyendo aquellos deportes para los cuales no hay inscriptos, ordenados por nombre de deporte (5 registros)

```sql
select d.*, count(i.numerosocio) as cantidad_inscriptos
from deportes d
left join inscriptos i on d.codigo = i.codigodeporte
group by d.codigo, d.nombre, d.dia
order by d.nombre;

```
12. Muestre las restricciones de "socios"

```sql
select constraint_name, constraint_type, table_name
from user_constraints
where table_name = 'SOCIOS';

```
13. Muestre las restricciones de "deportes"

```sql
select constraint_name, constraint_type, table_name
from user_constraints
where table_name = 'DEPORTES';

```
14. Obtenga información sobre la restricción "foreign key" de "deportes"

```sql
select constraint_name, table_name, r_constraint_name
from user_constraints
where table_name = 'DEPORTES' and constraint_type = 'R';

```
15. Muestre las restricciones de "profesores"

```sql
select constraint_name, constraint_type, table_name
from user_constraints
where table_name = 'PROFESORES';

```
16. Muestre las restricciones de "inscriptos"

```sql
select constraint_name, constraint_type, table_name
from user_constraints
where table_name = 'INSCRIPTOS';

```
17. Consulte "user_cons_columns" y analice la información retornada sobre las restricciones de "inscriptos"

```sql
select constraint_name,

```
18. Elimine un profesor al cual haga referencia algún registro de "deportes"

```sql
delete from profesores where documento = '23333333';

```
19. Vea qué sucedió con los registros de "deportes" cuyo "documentoprofesor" existía en "profesores"
Fue seteado a null porque la restricción "foreign key" sobre "documentoprofesor" de "deportes" fue definida "on delete set null".

```sql
select * from deportes;

```
20. Elimine un socio que esté inscripto

```sql
delete from socios where numero = 100;

```
21. Vea qué sucedió con los registros de "inscriptos" cuyo "numerosocio" existía en "socios"
Fue eliminado porque la restricción "foreign key" sobre "numerosocio" de "inscriptos" fue definida "on delete cascade".

```sql
select * from inscriptos;

```
22. Intente eliminar un deporte para el cual haya inscriptos
Mensaje de error porque la restricción "foreign key sobre "codigodeporte" de "inscriptos" fue establecida "no action".

```sql
delete from deportes where codigo = 1;

```
23. Intente eliminar la tabla "socios"
No puede eliminarse, mensaje de error, una "foreign key" sobre "inscriptos" hace referencia a esta tabla.

```sql
drop table socios;

```
24. Elimine la tabla "inscriptos"

```sql
drop table inscriptos;

```
25. Elimine la tabla "socios"

```sql
drop table socios;

```
26. Intente eliminar la tabla "profesores"
No puede eliminarse, mensaje de error, una "foreign key" sobre "deportes" hace referencia a esta tabla.

```sql
drop table profesores;

```
27. Elimine la tabla "deportes"

```sql
drop table deportes;

```
28. Elimine la tabla "profesores"
```sql
drop table profesores;

```