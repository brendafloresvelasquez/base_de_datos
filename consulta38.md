# 38. Restricción unique

## Practica de laboratorio

Trabajamos con la tabla "alumnos".

Eliminamos la tabla:

```sql
drop table alumnos;
```

Creamos la tabla:

```sql
create table alumnos(
    legajo char(4),
    apellido varchar2(20),
    nombre varchar2(20),
    documento char(8)
);
```

Agregamos una restricción "primary" para el campo "legajo":

```sql
alter table alumnos
add constraint PK_alumnos_legajo
primary key(legajo);
```

Agregamos una restricción "unique" para el campo "documento":

```sql
alter table alumnos
add constraint UQ_alumnos_documento
unique (documento);
```

Ingresamos algunos registros:

```sql
insert into alumnos values('A111','Lopez','Ana','22222222');
insert into alumnos values('A123','Garcia','Maria','23333333');
```

Intentamos ingresar un documento repetido:

```sql
insert into alumnos values('A230','Perez','Juan','23333333');
```

Aparece un mensaje de error indicando que se viola la restricción única.

Intentamos ingresar un legajo repetido:

```sql
insert into alumnos values('A123','Suarez','Silvana','30111222');
```

Aparece un mensaje de error indicando que se viola la restricción "primary key".

Veamos las restricciones de la tabla "alumnos" consultando el catálogo "user_constraints":

```sql
select *from user_constraints where table_name='ALUMNOS';
```

Aparecen las dos restricciones creadas anteriormente:

```sh
OWNER   CONSTRAINT_NAME       CONSTRAINT_TYPE  TABLE_NAME
--------------------------------------------------------------------------
SYSTEM  PK_ALUMNOS_LEGAJO     P                ALUMNOS
SYSTEM  UQ_ALUMNOS_DOCUMENTO  U                ALUMNOS
```

Veamos la información de "user_cons_columns":

```sql
select *from user_cons_columns where table_name='ALUMNOS';
```

Nos informa:

```sh
OWNER   CONSTRAINT_NAME        TABLE_NAME   COLUMN_NAME    POSITION
--------------------------------------------------------------------------------
SYSTEM  UQ_ALUMNOS_DOCUMENTO   ALUMNOS      DOCUMENTO      1
SYSTEM  PK_ALUMNOS_LEGAJO      ALUMNOS      LEGAJO         1
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table alumnos;

create table alumnos(
    legajo char(4),
    apellido varchar2(20),
    nombre varchar2(20),
    documento char(8)
);

alter table alumnos
add constraint PK_alumnos_legajo
primary key(legajo);

alter table alumnos
add constraint UQ_alumnos_documento
unique (documento);

insert into alumnos values('A111','Lopez','Ana','22222222');
insert into alumnos values('A123','Garcia','Maria','23333333');
insert into alumnos values('A230','Perez','Juan','23333333');
insert into alumnos values('A123','Suarez','Silvana','30111222');

select *from user_constraints where table_name='ALUMNOS';

select *from user_cons_columns where table_name='ALUMNOS';
```

## Ejercicios propuestos

Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".

1. Elimine la tabla:

```sql
drop table remis;
```

2. Cree la tabla con la siguiente estructura:

```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);
```

3. Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.
```sql
insert into remis (numero, patente, marca, modelo)
values (1, 'ABC123', 'Toyota', '2021');

insert into remis (numero, patente, marca, modelo)
values (2, 'DEF456', 'Ford', '2019');

insert into remis (numero, patente, marca, modelo)
values (3, 'ABC123', 'Chevrolet', '2020');

insert into remis (numero, patente, marca, modelo)
values (4, null, 'Honda', '2018');

```
4. Agregue una restricción "primary key" para el campo "numero".
```sql
alter table remis
add constraint pk_numero primary key (numero);

```
5. Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos.
No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.
```sql
alter table remis
add constraint uq_patente unique (patente);

```
6. Elimine el registro con patente duplicada y establezca la restricción.
Note que hay 1 registro con valor nulo en "patente".
```sql
delete from remis
where numero = 3;

alter table remis
add constraint uq_patente unique (patente);

```
7. Intente ingresar un registro con patente repetida (no lo permite)
```sql
insert into remis (numero, patente, marca, modelo)
values (5, 'ABC123', 'Nissan', '2017');
-- Oracle mostrará un mensaje de error indicando que la restricción "unique" ha sido violada.

```
8. Ingrese un registro con valor nulo para el campo "patente".
```sql
insert into remis (numero, patente, marca, modelo)
values (5, null, 'Hyundai', '2022');

```
9. Muestre la información de las restricciones consultando "user_constraints" y "user_cons_columns" y analice la información retornada (2 filas en cada consulta)
```sql
-- Consulta en user_constraints
select constraint_name, constraint_type
from user_constraints
where table_name = 'REMIS';

-- Consulta en user_cons_columns
select constraint_name, column_name
from user_cons_columns
where table_name = 'REMIS';

```